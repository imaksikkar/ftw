package com.indigohate;


import java.util.concurrent.ExecutionException;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentInformationActivity extends Activity {
	public String previousActivity = null;
    public String userdataResult;
    public String uid;
    public static final String EXTRA_UID = "com.indigohate.extra.UID";
    public static final String PREFS_NAME = "WheelsharePrefsFile";
    public String cardType;
    public String update = "set";
    public String firstname;
    public String lastname;
    public String cardnumber;
    public String expMonth;
    public String expYear;
    public String streetaddress;
    public String city;
    public String state;
    public String country;
    public String zip;
    
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle extras = getIntent().getExtras(); 
    		if(extras !=null) 
    		    previousActivity = extras.getString("LAST_ACTIVITY");
            if(previousActivity.equals("profile"))
            		asyncGetPaymentInfo();

    }
    
    public void onRadioButtonClicked(View view){
            boolean checked = ((RadioButton) view).isChecked();
            switch(view.getId()) {
            case R.id.radio_debit:
                    if(checked)
                            cardType = "debit";
                    break;
            case R.id.radio_credit:
                    if(checked)
                            cardType = "credit";
                    break;
            }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.payment_information, menu);
            return true;
    }
    
    public void onClickSave(View view) {
           //updatePaymentInfo(); 
    	asyncUpdatePaymentInfo();
    }
    
    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
    
    public String formatDateString(int year, int month, int day) {
            String monthStr = "" + month;
            String dayStr = "" + day;
            if(month < 10)
                    monthStr = "0" + month;
            if(day < 10)
                    dayStr = "0" + day;
            return(""+year+"-"+monthStr+"-"+dayStr);
    }
    
    private void asyncGetPaymentInfo()
    {
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    	uid = settings.getString("UID", "Invalid"); 
    	ServerRequestObject paymentData = new ServerRequestObject();
    	paymentData.setUrl("http://172.9.69.232/wheelshare/paymentinfo.php");
    	paymentData.putParameter("uid", uid);
    	ServerAsyncTask paymentInfo = new ServerAsyncTask();
    	try{
    		String str = paymentInfo.execute(paymentData).get();
    		//if(str == null){
            //if(str.length() < 1){
    			//update = "set";
            	//Payment info not yet set, so database is instructed to create new record
            	
            //}else{
            if(str.length() > 0){
            	update = "update";
            	//record exists, update, don't create new record
            
            	String[] paymentdataLines = str.split(",");
                setContentView(R.layout.activity_payment_information);
                
                EditText inputFirstName = (EditText) findViewById(R.id.text_card_first_name);
                EditText inputLastName = (EditText) findViewById(R.id.text_card_last_name);
                EditText inputCardNumber = (EditText) findViewById(R.id.text_card_number);
                EditText inputExpMonth = (EditText) findViewById(R.id.exp_date_month);
                EditText inputExpYear = (EditText) findViewById(R.id.exp_date_year);
                EditText inputStreetAddress = (EditText) findViewById(R.id.text_address);
                EditText inputCity = (EditText) findViewById(R.id.text_city);
                EditText inputState = (EditText) findViewById(R.id.text_state);
    			
                //EditText inputCountry = (EditText) findViewById(R.id.text_country);
                EditText inputZip = (EditText) findViewById(R.id.text_zip_code);
                
                if(paymentdataLines.length > 9){
	                inputFirstName.setText(paymentdataLines[0]);
	                inputLastName.setText(paymentdataLines[1]);
	                inputCardNumber.setText(paymentdataLines[2]);
	                inputExpMonth.setText(paymentdataLines[4]);
	                inputExpYear.setText(paymentdataLines[3]);
	                inputStreetAddress.setText(paymentdataLines[5]);
	                inputCity.setText(paymentdataLines[6]);
	                inputState.setText(paymentdataLines[7]);
	                inputZip.setText(paymentdataLines[8]);
	                cardType = paymentdataLines[9];
	                
                }else{
                	update="set";
                }
                //else{
                	//Log.d("paymentinfo", "not enough payment data: " + String.valueOf(paymentdataLines.length));
                //}
            }
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void asyncUpdatePaymentInfo()
    {
    	ServerRequestObject paymentInfo = new ServerRequestObject();
    	paymentInfo.setUrl("http://172.9.69.232/wheelshare/updatepaymentinfo.php");
    	EditText inputFirstName = (EditText) findViewById(R.id.text_card_first_name);
        EditText inputLastName = (EditText) findViewById(R.id.text_card_last_name);
        EditText inputCardNumber = (EditText) findViewById(R.id.text_card_number);
        EditText inputExpMonth = (EditText) findViewById(R.id.exp_date_month);
        EditText inputExpYear = (EditText) findViewById(R.id.exp_date_year);
        EditText inputStreetAddress = (EditText) findViewById(R.id.text_address);
        EditText inputCity = (EditText) findViewById(R.id.text_city);
        EditText inputState = (EditText) findViewById(R.id.text_state);
        EditText inputCountry = (EditText) findViewById(R.id.text_country);
        EditText inputZip = (EditText) findViewById(R.id.text_zip_code); 
        TextView errorMsg = (TextView) findViewById(R.id.payment_info_error_msg);
		
		firstname = inputFirstName.getText().toString();
	    lastname = inputLastName.getText().toString();
	    cardnumber = inputCardNumber.getText().toString();
	    expMonth = inputExpMonth.getText().toString();
	    expYear = inputExpYear.getText().toString();
	    streetaddress = inputStreetAddress.getText().toString();
	    city = inputCity.getText().toString();
	    state = inputState.getText().toString();
	    country = inputCountry.getText().toString();
	    zip = inputZip.getText().toString();
         
         if (!verifyCardNumber(cardnumber)) {
        	 errorMsg.setText("invalid card number");
        	 Log.d("registerpayment", "invalid card number");
         }
         else {		// valid card number
        	 Log.d("registerpayment", "valid card number");
        	 paymentInfo.putParameter("uid", uid);
             paymentInfo.putParameter("firstname", firstname);
             paymentInfo.putParameter("lastname", lastname);
             paymentInfo.putParameter("cardnumber", cardnumber);
             paymentInfo.putParameter("expirymonth", expMonth);
             paymentInfo.putParameter("expiryyear", expYear);
             paymentInfo.putParameter("type", cardType);
             paymentInfo.putParameter("address", streetaddress);
             paymentInfo.putParameter("city", city);
             paymentInfo.putParameter("state", state);
             paymentInfo.putParameter("zip", zip);
             paymentInfo.putParameter("update", update);
             ServerAsyncTask updatePayment = new ServerAsyncTask();
             try{
            	 String str = updatePayment.execute(paymentInfo).get();
            	 Toast.makeText(this,  "Result: " + str, Toast.LENGTH_LONG).show();
             }catch (InterruptedException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		} catch (ExecutionException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		}
             
            // switch to login or profile screen depending on what activity
            // this page was opened from (ProfileActivity or UserRegister)
            if(previousActivity.equals("profile")) {
	 			Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
	 			startActivity(intent);
				finish();
            }
            if(previousActivity.equals("register")) {
            	Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            	startActivity(intent);
    			finish();
            } 
            
        }
         
         
    }
    
    // Luhn algorithm to verify that card number is valid
    private boolean verifyCardNumber(String cardNumber) {
    	if(!(cardNumber.length() == 15 || cardNumber.length() == 16))
    		return (false);
        int sum = 0;
        boolean alternate = false;
        for (int i = cardNumber.length() - 1; i >= 0; i--)
        {
            int n = Integer.parseInt(cardNumber.substring(i, i + 1));
            if (alternate) 
            {
                n *= 2;
                if (n > 9)
                {
                   n = (n % 10) + 1;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        return (sum % 10 == 0);  
    }   
}

