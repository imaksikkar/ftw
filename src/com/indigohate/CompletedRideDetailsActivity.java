package com.indigohate;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class CompletedRideDetailsActivity extends Activity {

	public String rideDataItems[];
	public String uid;
	public String rideId;
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_completed_ride_details);
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);		
		uid = settings.getString("UID", "Invalid");
		
		
		String detailItems[] = null;
		String rideDetails = null;
		String rideData = getIntent().getStringExtra("ride");
		rideDataItems = rideData.split(" ");
		
		//rideDetails = getRideDetails(rideDataItems[0]);
		//detailItems = rideDetails.split(",");
		rideId = rideDataItems[0];
		detailItems = getRideDetails(rideDataItems[0]);
		
		TextView owner = (TextView) findViewById(R.id.details_owner);
		TextView date = (TextView) findViewById(R.id.details_date);
		TextView from = (TextView) findViewById(R.id.details_from);
		TextView to = (TextView) findViewById(R.id.details_to);
		TextView cost = (TextView) findViewById(R.id.details_cost);
		TextView seats = (TextView) findViewById(R.id.details_seats_left);
		
		Toast.makeText(getBaseContext(), detailItems[0], Toast.LENGTH_LONG).show();
		
		
		owner.setText(detailItems[1]);
		date.setText(detailItems[6]);
		from.setText(detailItems[4]);
		to.setText(detailItems[5]);
		cost.setText(detailItems[11]);
		seats.setText(detailItems[2]);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ride_detail, menu);
		return true;
	}

	private String[] getRideDetails(String rideId){
		HttpClient httpclient = new DefaultHttpClient();
		String url = "http://172.9.69.232/wheelshare/getridebyid.php?rideid=" + rideId;
		String str = null;
		HttpGet httpget = new HttpGet(url);
		
		HttpResponse response;
		String dataLines[] = null;
		try {
			response = httpclient.execute(httpget);
			HttpEntity httpEntity = response.getEntity();
			InputStream is = httpEntity.getContent();  
	        str = convertStreamToString(is);
	        
	        dataLines = str.split(",");
	                
	        
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		//return str;
		return dataLines;
	}
	
	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	private String joinRide(){
		String result = null;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://172.9.69.232/wheelshare/joinride.php");
		
		try{
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("rideid", rideId));
			
			nameValuePairs.add(new BasicNameValuePair("uid", uid));
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        HttpEntity httpEntity = response.getEntity();
			InputStream is = httpEntity.getContent();  
	        String str = convertStreamToString(is);
	        
	        Toast.makeText(this,  "Result Code: " + str, Toast.LENGTH_LONG).show();
	        result = str;
	    } catch (ClientProtocolException e) {
	    	e.printStackTrace();
	        
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public void onJoinRideButtonClick(View view){
		String result = joinRide();
		if(result == "0"){
			Toast.makeText(this,  "Error joining ride: " + result, Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(this,  "Joined Ride #" + rideId, Toast.LENGTH_LONG).show();
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
		}
	}
	
}