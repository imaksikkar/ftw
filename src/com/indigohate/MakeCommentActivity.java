package com.indigohate;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.view.KeyEvent;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import java.util.concurrent.ExecutionException;



public class MakeCommentActivity extends Activity {
	public String uid;
	public String targetUid;
	public String RatingValue;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_make_comment);
		addListenerOnRatingBar();
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		
		uid = settings.getString("UID", "Invalid");
		targetUid= getIntent().getStringExtra("UID");
		//Toast.makeText(getBaseContext(), uid, Toast.LENGTH_LONG).show();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.make_comment, menu);
		return true;
	}

	private void asyncSubmitReview()
	{
		ServerRequestObject reviewInfo = new ServerRequestObject();
		reviewInfo.setUrl("http://172.9.69.232/wheelshare/postcomment.php");
		EditText commentInput = (EditText) findViewById(R.id.text_comment);
		String comment = commentInput.getText().toString();
		reviewInfo.putParameter("comment", comment);
		reviewInfo.putParameter("commenter_id", uid);
		reviewInfo.putParameter("target_id", targetUid);
		reviewInfo.putParameter("star_rating", RatingValue);
		ServerAsyncTask submitReview = new ServerAsyncTask();
		try{
			String str = submitReview.execute(reviewInfo).get();
			Toast.makeText(this,  "Result: " + str, Toast.LENGTH_LONG).show();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	 public void addListenerOnRatingBar() {
		 
			RatingBar ratingBar = (RatingBar) findViewById(R.id.rating_bar);
		 
			//if rating value is changed,
			//display the current rating value in the result (textview) automatically
			ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
				public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
					RatingValue =(String.valueOf(rating));
		 
				}
			});
		  }
	
	public void onClickSubmitReview(View view){
		asyncSubmitReview();
		/*HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://172.9.69.232/wheelshare/postcomment.php");
		
		EditText commentInput = (EditText) findViewById(R.id.text_comment);
		String comment = commentInput.getText().toString();
		//Need to get uid of user being reviewed to insert in field 'target_id'
		//This activity should be started from the RideHistory activity
		//so that uid and the ride_id can be passed to it
		
		try{
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("comment", comment));
			nameValuePairs.add(new BasicNameValuePair("commenter_id", uid));
			//nameValuePairs.add(new BasicNameValuePair("target_id", target_id));
			//nameValuePairs.add(new BasicNameValuePair("ride_id", ride_id));
			
				        
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        
	        String str = convertStreamToString(response.getEntity().getContent()).toString();
	        
	        //loginErrorMsg.setText(status);
	        Toast.makeText(this,  "Result: " + str, Toast.LENGTH_LONG).show();
	    } catch (ClientProtocolException e) {
	        e.printStackTrace();
	    	//loginErrorMsg.setText(e.toString());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		//Intent intent = new Intent(this, ProfileActivity.class);
		Intent intent = new Intent(this, RideUsers.class);
		startActivity(intent);
		finish();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	        Intent intent= new Intent(this, RideUsers.class);
	        startActivity(intent);
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
}
