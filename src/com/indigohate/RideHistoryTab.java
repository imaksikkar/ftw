package com.indigohate;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.indigohate.classes.RideHistoryAdapter;
import com.indigohate.classes.RideTransaction;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.content.Intent;

@SuppressLint("ValidFragment")
public class RideHistoryTab extends Fragment{

	private static final String TAG = "ConfirmedList";

	private String mTag;
	//private MyAdapter mAdapter;
	private ArrayList<String> mItems;
	private LayoutInflater mInflater;
	private int mTotal;
	private int mPosition;
	ArrayList<RideTransaction> dataLinesList;
	//private ListView rideHistoryListView;
	public String uid;
	public String ridedataResult;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
	
	public RideHistoryTab(){
		
	}
	
	public RideHistoryTab(String tag){
		this.mTag = tag;
		Log.d(TAG, "Constructor: tag=" + tag);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View V = inflater.inflate(R.layout.history_tab_fragment, container, false);
        Log.d(TAG, "created");
          return V;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
		mInflater = LayoutInflater.from(getActivity());
		asyncGetRideHistory();
	}
	
    private void asyncGetRideHistory()
    {
    	dataLinesList = new ArrayList<RideTransaction>();
    	ServerRequestObject rideHistoryData = new ServerRequestObject();
    	SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
    	uid = settings.getString("UID", "Invalid");
    	rideHistoryData.setUrl("http://172.9.69.232/wheelshare/ridehistory.php");
    	rideHistoryData.putParameter("uid", uid);
    	ServerAsyncTask getRideHistory = new ServerAsyncTask();
    	try{
    		String str = getRideHistory.execute(rideHistoryData).get();
    		if(str==null || str ==""){
    			Log.d("history", "invalid http return");
    			Toast.makeText(getActivity(), "Unavailable Network", Toast.LENGTH_LONG).show();
    		}
    		else{
	    		String[] ridedataLines = str.split(";");
	    		Log.d(TAG,"string is " + str);
	    		for(String ride:ridedataLines){
	    			if(ride.startsWith("EOF"))
	    				break;
	    			RideTransaction rideTransaction = RideTransaction.stringToObject(ride);
	    			if(rideTransaction == null)
	    				Log.d("history", "invalid data");
	    			else
	    				dataLinesList.add(rideTransaction);
	    		}
    		}
    		/*try{
    			
    			
    			
    			 for(int i=0; i < ridedataLines.length; i++){
    				 if(ridedataLines[i].contains("EOF"))
    					 break;
    				 //RideTransaction rideTransaction = RideTransaction.stringToObject(results)
    				dataLinesList.add(ridedataLines[i]);
    			 }
    			
    			}catch(NullPointerException e){
    				e.printStackTrace();
    			}
    		*/
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Log.d(TAG, "length of data is " + dataLinesList.size());
    	ListView searchResultsList = (ListView)getView().findViewById(R.id.historyListView);
		RideHistoryAdapter adapter = new RideHistoryAdapter(getActivity(),
				R.layout.ridehistory_listview_item,
				dataLinesList.toArray(new RideTransaction[dataLinesList.size()]));
		searchResultsList.setOnItemClickListener(new OnItemClickListener()
			{
				@Override 
				public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
			    { 
					//Navigate to the ride details here
					//String data = ((TextView)arg1.findViewById(R.id.basicTextItem)).getText().toString();
			       // Toast.makeText(getActivity().getApplicationContext(), "" + position + " " + data, Toast.LENGTH_LONG).show();
					if(position == 0){
						
					}
					else{
						Intent intent = new Intent(getActivity(), RideUsers.class);
				        Bundle extras= new Bundle();
				        RideTransaction rideTransaction = dataLinesList.get(position-1);
				        extras.putString("rid", rideTransaction.rideId);
				        intent.putExtras(extras);
						getActivity().startActivity(intent);
					}
			    }
			}
		);
		if(getView().findViewById(R.id.historyEmptyView) == null){
			Log.d(TAG,"uh oh");
		}
		View header = (View)getActivity().getLayoutInflater().inflate(R.layout.ridehistory_listview_header, null);
    	searchResultsList.addHeaderView(header);
		searchResultsList.setEmptyView(getView().findViewById(R.id.historyEmptyView));
		searchResultsList.setAdapter(adapter);
    }
    
   /* public class RideHistoryArrayAdapter extends ArrayAdapter<String>{
    	Context context;
    	int layoutResourceId;
    	String data[] = null;
    	private String TAG = "ridehistoryarrayadapter";
    	
    	public RideHistoryArrayAdapter(Context context, int layoutResourceId, String[] data){
    		super(context,layoutResourceId,data);
    		this.context=context;
    		this.layoutResourceId = layoutResourceId;
    		this.data = data;
    	}
    	
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent){
    		View row = convertView;
    		RideHistoryHolder holder = null;
    		if(row == null){
    			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
    			row = inflater.inflate(layoutResourceId,parent,false);
    			holder = new RideHistoryHolder();
    			holder.textResult = (TextView)row.findViewById(R.id.basicTextItem);
    			row.setTag(holder);
    		}
    		else{
    			holder = (RideHistoryHolder)row.getTag();
    		}
    		
    		String result = data[position];
    		holder.textResult.setText(result);
    		return row;
    	}
    	
    	class RideHistoryHolder{
    		TextView textResult;
    	}
    }*/
}
