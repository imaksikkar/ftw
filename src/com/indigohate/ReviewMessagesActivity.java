package com.indigohate;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.indigohate.dummy.DummyContent;
import com.indigohate.Ride;
import com.indigohate.RideContent;
import com.indigohate.SearchResultsListAdapter;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;


public class ReviewMessagesActivity extends ListActivity {
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	public String uid;
	private ListView searchResultsList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_review_messages);
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	
		uid = settings.getString("UID", "Invalid");
		
		ServerRequestObject messageRequest = new ServerRequestObject();
		
		String url = "http://172.9.69.232/wheelshare/getmessages.php";
		messageRequest.setUrl(url);
		messageRequest.putParameter("uid", uid);
		
		
		ServerAsyncTask getMessages = new ServerAsyncTask();
		
		String result;
		try {
			result = getMessages.execute(messageRequest).get();
			ArrayList<String> dataLinesList = new ArrayList<String>();
			
			String messages[] = result.split(";");
			
			for(int i=0; i < messages.length; i++){
				dataLinesList.add(messages[i]);
			}
			
			searchResultsList = getListView();
			
			SearchResultsListAdapter adapter = new SearchResultsListAdapter(this,
					android.R.layout.simple_list_item_1,
					dataLinesList);
			searchResultsList.setAdapter(adapter);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.review_messages, menu);
		return true;
	}

}
