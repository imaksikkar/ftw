package com.indigohate;


import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;


public class ServerRequestObject extends Object{
	
	private String targetUrl;
	private List<NameValuePair> parameters;
	
	public ServerRequestObject(String url, List<NameValuePair> nameValuePairs){
		targetUrl = url;
		parameters = nameValuePairs;
		
	}
	
	public ServerRequestObject(){
		parameters = new ArrayList<NameValuePair>(2);
	}
	
	public void setUrl(String url){
		this.targetUrl = url;
	}
	
	public String getUrl(){
		return targetUrl;
	}
	
	public void setParameters(List<NameValuePair> nameValuePairs){
		this.parameters = nameValuePairs;
	}
	
	public void putParameter(String key, String value){
		parameters.add(new BasicNameValuePair(key, value));
	}
	
	public String getParameter(String key){
		int index = parameters.indexOf(key);
		String value = parameters.get(index).toString();
		
		return value;
	}
	
	public List<NameValuePair> getParameters(){
		return parameters;
	}
}