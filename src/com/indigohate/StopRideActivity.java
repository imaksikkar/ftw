package com.indigohate;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

public class StopRideActivity extends Activity {
	public String uid;
	public String rideId;
	public String fromLocation;
	public String toLocation;
	
	private LocationManager mgr;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stop_ride);
		
		TextView fromLoc = (TextView) findViewById(R.id.from_location);
	    TextView toLoc = (TextView) findViewById(R.id.destination_location);
	    
		Bundle extras = getIntent().getExtras(); 
		if(extras !=null) {
		    rideId = extras.getString("RIDE_ID");
		    fromLocation = extras.getString("FROM");
		    toLocation = extras.getString("TO");
		    fromLoc.setText(fromLocation);
		    toLoc.setText(toLocation);
		}
		
		mgr=(LocationManager)getSystemService(LOCATION_SERVICE);
	    
	    mgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                3600, 2,
                onLocationChange);
	    showCurrentLocation();
	    
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stop_ride, menu);
		return true;
	}
	
	public void onClickStopRideButton(View view){
		asyncStopRide();
		Intent intent = new Intent(this, SubmitPaymentActivity.class);
		intent.putExtra("RIDE_ID", rideId);
		startActivity(intent);
		finish();
	}
	
	 private void asyncStopRide()
	    {
	    
	    	ServerRequestObject stopRide = new ServerRequestObject();
	    	stopRide.setUrl("http://172.9.69.232/wheelshare/startstopride.php");

	    	stopRide.putParameter("uid", uid);
	    	stopRide.putParameter("rideid", rideId);
	    	stopRide.putParameter("action", "start");
	    	
	    	ServerAsyncTask requestRide = new ServerAsyncTask();
	    	try{
	    		String str = requestRide.execute(stopRide).get();
		    	Toast.makeText(this,  "Result Code: " + str, Toast.LENGTH_LONG).show();
	    	
	    	}catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	protected void showCurrentLocation() {
		List<Address> addresses;
		Address address;
		String locality = "Currently in: ";
    	Location location = mgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
    	Geocoder translator = new Geocoder(this);
    	
    	try {
			addresses = translator.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
			address = addresses.get(0);
	    	locality = locality + address.getLocality();
	    	TextView currentLocation = (TextView) this.findViewById(R.id.endLocationText);
		    currentLocation.setText(locality);
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	LocationListener onLocationChange=new LocationListener() {
	    public void onLocationChanged(Location location) {
	     
			@SuppressWarnings("unused")
	    	Location targetLocation = mgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);//null;
	    			
	    	showCurrentLocation();
	    	//Toast.makeText(MainGpsActivity.this, message, Toast.LENGTH_LONG).show();	    
	    }
	    
	    public void onProviderDisabled(String provider) {
	      // required for interface, not used
	    
	    }
	    
	    public void onProviderEnabled(String provider) {
	      // required for interface, not used
	    	
	    }
	    
	    public void onStatusChanged(String provider, int status,
	                                  Bundle extras) {
	      // required for interface, not used
	    	
	    }
	  };
	
}
