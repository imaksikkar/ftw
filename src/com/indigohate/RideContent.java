package com.indigohate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RideContent {

    public static class RideItem {

        public String id;
        public String content;

        public RideItem(String id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }

    public List<RideItem> ITEMS = new ArrayList<RideItem>();
    public static Map<String, RideItem> ITEM_MAP = new HashMap<String, RideItem>();

    public void addItem(RideItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }
}
