package com.indigohate.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyContent {

    public static class DummyItem {

        public String id;
        public String content;

        public DummyItem(String id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }

    public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();
    public static Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    static {
        addItem(new DummyItem("1", "Departs 8:00 to San Francisco"));
        addItem(new DummyItem("2", "Departs 9:00 to Berkeley"));
        addItem(new DummyItem("3", "Departs 9:30 to Danville"));
        addItem(new DummyItem("4", "Departs 10:00 to Walnut Creek"));
        addItem(new DummyItem("5", "Departs 10:00 to Pleasant Hill"));
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }
}
