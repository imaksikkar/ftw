package com.indigohate;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LostPasswordActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lost_password);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lost_password, menu);
		return true;
	}
	
	public void onSendClicked(View view){
		// do something to send temp password to entered email
		EditText usernameInput = (EditText) findViewById(R.id.lost_username);
		String username = usernameInput.getText().toString();
		
		ServerRequestObject random = new ServerRequestObject();
		random.setUrl("http://172.9.69.232/wheelshare/lostpassword.php");
		random.putParameter("username", username);
		ServerAsyncTask sendRequest = new ServerAsyncTask();
		
		sendRequest.execute(random);
		Toast.makeText(this, "Check your email", Toast.LENGTH_LONG).show();
		
		Intent intent = new Intent(this, LoginActivity.class);
		intent.putExtra("USERNAME", username);
		startActivity(intent);
	}
	
}
