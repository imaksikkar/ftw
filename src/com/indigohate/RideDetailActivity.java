package com.indigohate;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class RideDetailActivity extends Activity {

	public String rideDataItems[];
	public String uid;
	public String rideId;
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ride_detail);
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);		
		uid = settings.getString("UID", "Invalid");
		
		
		String detailItems[] = null;
		//String rideDetails = null;
		//String rideData = getIntent().getStringExtra("ride");
		//rideDataItems = rideData.split(" ");
		String rId = getIntent().getStringExtra("ride");
		//rideDetails = getRideDetails(rideDataItems[0]);
		//detailItems = rideDetails.split(",");
		rideId = rId;
		detailItems = getRideDetails(rideId);
		
		TextView owner = (TextView) findViewById(R.id.details_owner);
		TextView date = (TextView) findViewById(R.id.details_date);
		TextView from = (TextView) findViewById(R.id.details_from);
		TextView to = (TextView) findViewById(R.id.details_to);
		TextView cost = (TextView) findViewById(R.id.details_cost);
		TextView seats = (TextView) findViewById(R.id.details_seats_left);
		
		Toast.makeText(getBaseContext(), detailItems[0], Toast.LENGTH_LONG).show();
		
		if(detailItems.length > 11){
		owner.setText(detailItems[1]);
		date.setText(detailItems[6]);
		from.setText(detailItems[4]);
		to.setText(detailItems[5]);
		cost.setText(detailItems[11]);
		seats.setText(detailItems[2]);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ride_detail, menu);
		return true;
	}
/*
	private String[] getRideDetails(String rideId){
		HttpClient httpclient = new DefaultHttpClient();
		String url = "http://172.9.69.232/wheelshare/getridebyid.php?rideid=" + rideId;
		String str = null;
		HttpGet httpget = new HttpGet(url);
		
		HttpResponse response;
		String dataLines[] = null;
		try {
			response = httpclient.execute(httpget);
			HttpEntity httpEntity = response.getEntity();
			InputStream is = httpEntity.getContent();  
	        str = convertStreamToString(is);
	        
	        dataLines = str.split(",");
	                
	        
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		//return str;
		return dataLines;
	}*/
	private String[] getRideDetails(String rideId){
		String url = "http://172.9.69.232/wheelshare/getridebyidpost.php";
		
		ServerRequestObject details = new ServerRequestObject();
		ServerAsyncTask getDetails = new ServerAsyncTask();
		
		details.setUrl(url);
		details.putParameter("rideid", rideId);
		
		String resultLines[] = null;
		String result = null;
		try {
			result = getDetails.execute(details).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(result != null){
			resultLines = result.split(",");
		}
		
		return resultLines;
		
	}
	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	// right now this isn't "pending". It just adds the user to the ride
	private String joinRidePending(){
		String result = null;
		
		ServerRequestObject join = new ServerRequestObject();
		ServerAsyncTask doJoin = new ServerAsyncTask();
		
		join.setUrl("http://172.9.69.232/wheelshare/joinride.php");
		join.putParameter("rideid", rideId);
		join.putParameter("uid", uid);
		
		try {
			result = doJoin.execute(join).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return result;
	}
	
	public void onJoinRideButtonClick(View view){
		String result = joinRidePending();
		if(result == "0"){
			Toast.makeText(this,  "Error joining ride: " + result, Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(this,  "Joined Ride #" + rideId, Toast.LENGTH_LONG).show();
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
		}
	}
	
}
