package com.indigohate;


import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import com.indigohate.classes.InputCheck;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class RideOfferActivity extends Activity {
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	public String uid;
	private int startHour; // = -999; 
	private int startMinute; //= -999;
	private int startYear, startMonth, startDay;
	String startLocation;
	String endLocation;
	String numberSeats;
	String offerCost;
	String startTimeStr;
	String offerDate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		uid = settings.getString("UID", "Invalid");
		setContentView(R.layout.activity_ride_offer);
		
		TimePicker startTimePicker = (TimePicker) findViewById(R.id.start_time_picker);
		startTimePicker.setOnTimeChangedListener(mStartTimeChangedListener);
		
		DatePicker startDatePicker = (DatePicker) findViewById(R.id.start_date_picker);
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		startDatePicker.init(year, month, day, mStartDateChangedListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ride_offer, menu);
		return true;
	}
	
	public void onClickSubmitOfferButton(View view){		
		//submitRide();
		asyncSubmitRide();
		
	}
	
	private TimePicker.OnTimeChangedListener mStartTimeChangedListener =
		new TimePicker.OnTimeChangedListener() {
				
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				startHour = hourOfDay;
				startMinute = minute;		
			}
		};
		
	private DatePicker.OnDateChangedListener mStartDateChangedListener = 
			new DatePicker.OnDateChangedListener() {
				
				@Override
				public void onDateChanged(DatePicker view, int year, int monthOfYear,
						int dayOfMonth) {
					startMonth = monthOfYear;
					startYear = year;
					startDay = dayOfMonth;
				}
			};
	
	private void asyncSubmitRide()
	{
		ServerRequestObject rideData = new ServerRequestObject();
		rideData.setUrl("http://172.9.69.232/wheelshare/offerride.php");
		
		EditText startLocationInput = (EditText) findViewById(R.id.offer_start_location);
		EditText endLocationInput = (EditText) findViewById(R.id.offer_end_location);
		EditText numberSeatsInput = (EditText) findViewById(R.id.num_seats_available);
		EditText offerCostInput = (EditText) findViewById(R.id.offer_cost);
		
		
		startLocation = startLocationInput.getText().toString();
		endLocation = endLocationInput.getText().toString();
		numberSeats = numberSeatsInput.getText().toString();
		offerCost = offerCostInput.getText().toString();
		startTimeStr = formatTimeString(startHour, startMinute);
		offerDate = formatDateString(startYear, startMonth, startDay);
		if( InputCheck.isValidString(this, startLocation, endLocation) && InputCheck.isStringInputValid(this, numberSeats, offerCost, startTimeStr, offerDate)){
			rideData.putParameter("fromlocation", startLocation);
			rideData.putParameter("todestination", endLocation);
			rideData.putParameter("fromdate", offerDate);
			rideData.putParameter("ownerid", uid);
			rideData.putParameter("costpermile", offerCost);
			rideData.putParameter("numberseats", numberSeats);
			rideData.putParameter("starttime", startTimeStr);
			
			ServerAsyncTask postRide = new ServerAsyncTask();
			try{
				String str = postRide.execute(rideData).get();
				Toast.makeText(this,  "Result Code: " + str, Toast.LENGTH_LONG).show();
				Intent intent = new Intent(this, SearchForRequestsActivity.class);
				Bundle extras = new Bundle();
				extras.putString("startLocation", startLocation);
				extras.putString("endLocation", endLocation);
				extras.putString("rideId", str);
				intent.putExtras(extras);
				
				
				startActivity(intent);
			}catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
/*	private void asyncSearchForRequests() {
		EditText startLocationInput = (EditText) findViewById(R.id.offer_start_location);
		EditText endLocationInput = (EditText) findViewById(R.id.offer_end_location);
		
		String startLocation = startLocationInput.getText().toString();
		String endLocation = endLocationInput.getText().toString();
		
		if(InputCheck.isValidString(this, startLocation, endLocation)){
			String startTimeStr = formatTimeString(startHour, startMinute);
			String requestDate = formatDateString(startYear, startMonth, startDay);
			
			Intent intent = new Intent(this, SearchResultsListActivity.class);
			
			//Bundle extras = intent.getExtras();
			Bundle extras = new Bundle();
			extras.putString("wantType", "request");
			extras.putString("startLocation", startLocation);
			extras.putString("endLocation", endLocation);
			extras.putString("rideDate", requestDate);
			extras.putString("rideTime", startTimeStr);
			
			intent.putExtras(extras);
			
			startActivity(intent);
		}
	}
*/
	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	public String formatDateString(int year, int month, int day) {
		String monthStr = "" + month;
		String dayStr = "" + day;
		if(month < 10)
			monthStr = "0" + month;
		if(day < 10)
			dayStr = "0" + day;
		return(""+year+"-"+monthStr+"-"+dayStr);
	}
	
	public String formatTimeString(int hour, int minute) {
		String hourStr = "" + hour;
		String minuteStr = "" + minute;
		if(hour < 10)
			hourStr = "0" + hour;
		if(minute < 10)
			minuteStr = "0" + minute;
		return(""+hourStr+":"+minuteStr+":"+"00");
	}
}
	

