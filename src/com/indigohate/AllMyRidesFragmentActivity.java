package com.indigohate;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.indigohate.classes.JoinedRide;
import com.indigohate.classes.PendingOffer;
import com.indigohate.classes.PendingRequest;
import com.indigohate.classes.PendingRide;

import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;
import android.widget.Toast;

public class AllMyRidesFragmentActivity extends FragmentActivity {

	public static final String TAG = "FragmentActivity";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	public ArrayList<String> allRideData;
	//public ArrayList<String> joinedRides = new ArrayList<String>();
	public ArrayList<JoinedRide> joinedRides = new ArrayList<JoinedRide>();
	//public ArrayList<String> pendingRides = new ArrayList<String>();
	public ArrayList<PendingRide> pendingRides = new ArrayList<PendingRide>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_my_rides_fragment);
		allRideData = asyncGetMyRides();
		if(allRideData == null){
			Log.d(TAG, "invalid data");
		}
		else{
			Log.d(TAG, "valid data");
			for(String ride:allRideData){
				Log.d("list","this is the ride " + ride);
			}
			sortRides();
		}
	}
/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.all_my_rides, menu);
		return true;
	}*/
	
	
    private ArrayList<String> asyncGetMyRides()
    {
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    	String uid = settings.getString("UID", "Invalid");
		//allridesdetails.php
    	String url = "http://172.9.69.232/wheelshare/allridesdetails.php";
    	//String url = "http://172.9.69.232/wheelshare/allridespost.php";
    	ServerRequestObject rideData = new ServerRequestObject();
    	rideData.setUrl(url);
    	rideData.putParameter("uid", uid);
    	ArrayList<String> dataLinesList = new ArrayList<String>();
    	ServerAsyncTask getRides = new ServerAsyncTask();
    	try{
    		String str = getRides.execute(rideData).get();
    		if(str ==null || str== ""){
    			Toast.makeText(this, "Unavailable Network", Toast.LENGTH_LONG).show();
    		}
    		else{
	    		String[] ridedataLines = str.split(";");
	    		try{
	    			 for(int i=0; i < ridedataLines.length; i++){
	    				 if(ridedataLines[i].startsWith("EOF"))
	    					 break;
	    				dataLinesList.add(ridedataLines[i]);
	    			 }
	    			
	    			}catch(NullPointerException e){
	    				e.printStackTrace();
	    				//maybe change this to a conditional?
	    			}
    		}
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	/*
		ListView searchResultsList = (ListView)findViewById(R.id.confirmedListView);
		//searchResultsList = (ListView)findViewById(R.id.rideHistoryListView);
		
		SearchResultsListAdapter adapter = new SearchResultsListAdapter(this,
				//R.layout.listview_item_row, 
				android.R.layout.simple_list_item_1,
				dataLinesList);
		
		searchResultsList.setAdapter(adapter);*/
    	if(dataLinesList.size() > 1){
    		return dataLinesList;
    	}
    	return null;
    }

    //allridesdetails.php , offer, request, joined , 
    public void sortRides(){
    	for(String ride:allRideData){
    		String parsedString[] = ride.split(",");
    		Log.d("ORR","ride is " + ride);
    		if(ride != null){
	    		if(ride.contains("Joined")){
	    			JoinedRide joinedRide = JoinedRide.stringtoRide(parsedString);
	    			if (joinedRide !=null)
	    				joinedRides.add(JoinedRide.stringtoRide(parsedString));
	    		}
	    		else{
	    			PendingRide pendingRide;
	    			if(parsedString[1].contains("Offer")){
	    				Log.d(TAG,"making an offer");
	    				pendingRide = (PendingRide)PendingOffer.stringToOffer(parsedString);
	    				if(pendingRide != null)
	    					pendingRides.add((PendingRide)PendingOffer.stringToOffer(parsedString));
	    			}
	    			else if (parsedString[1].contains("Request")){
	    				Log.d(TAG,"making a request");
	    				pendingRide = (PendingRide)PendingRequest.stringToRequest(parsedString);
	    				if (pendingRide != null);
	    					pendingRides.add((PendingRide)PendingRequest.stringToRequest(parsedString));
	    			}
	    			else{
	    				Log.d(TAG,"uh oh status is " + parsedString[1]);
	    			}
	    			//pendingRides.add(ride);
	    		}
    		}
    	}
    }
    
    public JoinedRide[] getJoinedRides(){
    	return joinedRides.toArray(new JoinedRide[joinedRides.size()]);
    }
    
    public PendingRide[] getPendingRides(){
    	Log.d(TAG,"length of rides is " + pendingRides.size());
    	return pendingRides.toArray(new PendingRide[pendingRides.size()]);
    }
    
}
