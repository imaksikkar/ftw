package com.indigohate;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.concurrent.ExecutionException;

import com.indigohate.classes.InputCheck;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.*;

//import com.indigohate.userdataendpoint.model.UserData;


public class UserRegister extends Activity {
	String gender;
	Button btnRegister;
	Button btnLinkToLogin;
	EditText inputFirstName;
	EditText inputLastName;
	EditText inputUsername;
	EditText inputEmail;
	EditText inputPassword;
	EditText inputConfirmPassword;
	EditText inputStreetAddress;
	EditText inputCity;
	EditText inputState;
	EditText inputZip;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_register);
		
		
		
		 btnRegister = (Button) findViewById(R.id.btn_register);
		 btnLinkToLogin = (Button) findViewById(R.id.btn_link_to_login);
		 inputFirstName = (EditText) findViewById(R.id.user_firstname);
		 inputLastName = (EditText) findViewById(R.id.user_lastname);
		 inputUsername = (EditText) findViewById(R.id.user_username);
		 inputEmail = (EditText) findViewById(R.id.user_email);
		 inputPassword = (EditText) findViewById(R.id.user_password);
		 inputConfirmPassword = (EditText) findViewById(R.id.user_password_confirm);
		 inputStreetAddress = (EditText) findViewById(R.id.user_street);
		 inputCity = (EditText) findViewById(R.id.user_city);
		 inputState = (EditText) findViewById(R.id.user_state);
		 inputZip = (EditText) findViewById(R.id.user_zip);

		
		
		// Listening to Login Screen link
		btnLinkToLogin.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
							// Closing registration screen
				// Switching to Login Screen/closing register screen
				Intent i = new Intent(getApplicationContext(), PaymentInformationActivity.class);
				startActivity(i);
				// Close registration View
				finish();
			}

		});
		
		btnRegister.setOnClickListener(new View.OnClickListener() {
			

			@Override
			public void onClick(View view) {
				asyncRegisterUserData(view);
				/*// setUserInfo(view);
				Intent intent = new Intent(getApplicationContext(), PaymentInformationActivity.class);
				intent.putExtra("LAST_ACTIVITY", "register");
				startActivity(intent);
				// Close registration View
				finish();*/
			}
		});
	}
	
	
	private void asyncRegisterUserData(View v)
	{
		 inputFirstName = (EditText) findViewById(R.id.user_firstname);
		 inputLastName = (EditText) findViewById(R.id.user_lastname);
		 inputUsername = (EditText) findViewById(R.id.user_username);
		 inputEmail = (EditText) findViewById(R.id.user_email);
		 inputPassword = (EditText) findViewById(R.id.user_password);
		 inputConfirmPassword = (EditText) findViewById(R.id.user_password_confirm);
		 inputStreetAddress = (EditText) findViewById(R.id.user_street);
		 inputCity = (EditText) findViewById(R.id.user_city);
		 inputState = (EditText) findViewById(R.id.user_state);
		 inputZip = (EditText) findViewById(R.id.user_zip);
		 
		String firstname = inputFirstName.getText().toString();
		String lastname = inputLastName.getText().toString();
		String username = inputUsername.getText().toString();
		String email = inputEmail.getText().toString();
		String password = inputPassword.getText().toString();
		@SuppressWarnings("unused")
		String confirmPassword = inputConfirmPassword.getText().toString();
		String street = inputStreetAddress.getText().toString();
		String city = inputCity.getText().toString();
		String state = inputState.getText().toString();
		String zip = inputZip.getText().toString();
		 if(InputCheck.isStringInputValid(this,firstname,lastname,username,email,password,confirmPassword,city,state,zip) && InputCheck.isEmailValid(this, email)){

				ServerRequestObject registerData = new ServerRequestObject();
				registerData.setUrl("http://172.9.69.232/wheelshare/register.php");
				registerData.putParameter("firstname", firstname);
				registerData.putParameter("lastname", lastname);
				registerData.putParameter("username", username);
				registerData.putParameter("password", password);
				registerData.putParameter("email", email);
				registerData.putParameter("street", street);
				registerData.putParameter("city", city);
				registerData.putParameter("state", state);
				registerData.putParameter("zip", zip);
				registerData.putParameter("sex", gender);
				
				ServerAsyncTask register = new ServerAsyncTask();
				try{
					String str = register.execute(registerData).get();
					Toast.makeText(this,  "Result: " + str, Toast.LENGTH_LONG).show();
					Intent intent = new Intent(getApplicationContext(), PaymentInformationActivity.class);
					intent.putExtra("LAST_ACTIVITY", "register");
					startActivity(intent);
					// Close registration View
					finish();
				}catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 }
	}
	
	public void onRadioButtonClicked(View view){
		boolean checked = ((RadioButton) view).isChecked();
		switch(view.getId()) {
		case R.id.radio_male:
			if(checked)
				gender = "male";
			break;
		case R.id.radio_female:
			if(checked)
				gender = "female";
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_register, menu);
		return true;
	}


	//Apparently the context.MODE_WORLD_READER is risky. double check it
	//I suppressed just to away with the warning
	@SuppressLint("WorldReadableFiles")
	public void writeUserDataFile(String value, Context context){
		try{
			FileOutputStream fOut = context.openFileOutput("userdatafile.txt", Context.MODE_WORLD_READABLE);
	        OutputStreamWriter osw = new OutputStreamWriter(fOut);
			
	        // Write the string to the file
			
	        osw.write(value);
	
	        // save and close
	        osw.flush();
	        osw.close();
		}catch (IOException e){
			e.printStackTrace();
		}
	}

}
