package com.indigohate;

import java.util.concurrent.ExecutionException;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class SubmitPaymentActivity extends Activity {
    public String userdataResult;
    public String uid;
    public static final String EXTRA_UID = "com.indigohate.extra.UID";
    public static final String PREFS_NAME = "WheelsharePrefsFile";
    public String cardType;
    public String update;
    public String firstname;
    public String lastname;
    public String cardnumber;
    public String expMonth;
    public String expYear;
    public String streetaddress;
    public String city;
    public String state;
    public String country;
    public String zip;
    
    public String ownerId;
    public String rideDate;
    public String rideCost;
    public String rideId;
    public String completed;
    public boolean gotPaymentInfo=false;
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_submit_payment);
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
      	uid = settings.getString("UID", "Invalid");
		
		Bundle extras = getIntent().getExtras(); 
		if(extras !=null) {
		    rideId = extras.getString("RIDE_ID");
		}
		gotPaymentInfo = asyncGetPaymentInfo();
		String[] rideDetails = getRideDetails(rideId);
		if(rideDetails.length > 11) {
			ownerId = rideDetails[13];
			rideDate = rideDetails[6];
			rideCost = rideDetails[11];
		}
		completed = "yes";		// temp default value
		TextView confirmMessage = (TextView) findViewById(R.id.confirm_payment_msg);
		confirmMessage.setText("Please confirm your payment of $" + rideCost 
				+ " for this completed ride");

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.submit_payment, menu);
		return true;
	}

	private String[] getRideDetails(String rideId){
		String url = "http://172.9.69.232/wheelshare/getridebyidpost.php";
		
		ServerRequestObject details = new ServerRequestObject();
		ServerAsyncTask getDetails = new ServerAsyncTask();
		
		details.setUrl(url);
		details.putParameter("rideid", rideId);
		
		String resultLines[] = null;
		String result = null;
		try {
			result = getDetails.execute(details).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(result != null){
			resultLines = result.split(",");
		}
		else{
			Toast.makeText(this, "Unavailable Network", Toast.LENGTH_LONG).show();
		}
		
		return resultLines;
		
	}
	
	  //todo: fix this 
	  private boolean asyncGetPaymentInfo()
      {
    
      	ServerRequestObject paymentData = new ServerRequestObject();
      	paymentData.setUrl("http://172.9.69.232/wheelshare/paymentinfo.php");
      	paymentData.putParameter("uid", uid);
      	ServerAsyncTask paymentInfo = new ServerAsyncTask();
      	try{
      		String str = paymentInfo.execute(paymentData).get();
      		if(str != null){
      
              	String[] paymentdataLines = str.split(",");
                  setContentView(R.layout.activity_submit_payment);
       
                  if(paymentdataLines.length > 9){
                	  return true;		// got payment information successfully
                  }
      		
                  else{
                  	Log.d("paymentinfo", "not enough payment data: " + String.valueOf(paymentdataLines.length));
                  	return false;		// failed: not enough payment data
                  }
      		}
      		else{
      			Toast.makeText(this, "Unavailable Network", Toast.LENGTH_LONG).show();
      		}
      	}catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (ExecutionException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
      	return false;
      }
	  
	  public void onClickSubmit(View view){
			if(gotPaymentInfo) {
				submitPayment();
				Intent intent = new Intent(this, PaymentConfirmationActivity.class);
				startActivity(intent);	
				finish();
			}
			
			// move this stuff inside the if-stmt once asyncGetPaymentInfo() is working
//			Intent intent = new Intent(this, PaymentConfirmationActivity.class);
//			startActivity(intent);		
	  }
	  
	  //todo: fix this 
	  public void submitPayment() {
	    ServerRequestObject payment = new ServerRequestObject();
  	    payment.setUrl("http://172.9.69.232/wheelshare/dopayment.php");
  	    payment.putParameter("rideid", rideId);
  	    payment.putParameter("payerid", uid);
  	    payment.putParameter("payeeid", ownerId);
  	    payment.putParameter("amount", rideCost);
  	    //payment.putParameter("completed", completed);
  	    payment.putParameter("date", rideDate);
      	
        ServerAsyncTask doPayment = new ServerAsyncTask();
        try{
	       	 String str = doPayment.execute(payment).get();
	       	 Toast.makeText(this,  "Result: " + str, Toast.LENGTH_LONG).show();
        }catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
}
