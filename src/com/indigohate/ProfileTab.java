package com.indigohate;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ValidFragment")
public class ProfileTab extends Fragment{

	public String uid;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.PROFILE";
	
	
	private static final String TAG = "ProfileTab";
	private LayoutInflater mInflater;
	private String mTag;

	public ProfileTab(){
		
	}
	
	public ProfileTab(String tag){
		this.mTag=tag;
		Log.d(TAG, "Constructor: tag=" + tag);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View V = inflater.inflate(R.layout.profile_tab_fragment, container, false);
        Log.d(TAG, "created");
          return V;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
		mInflater = LayoutInflater.from(getActivity());
		asyncGetProfileData();
	}
	
	
	public void asyncUpdateContact()
	{
		ServerRequestObject updateProfileData = new ServerRequestObject();
		updateProfileData.setUrl("http://172.9.69.232/wheelshare/updateprofile.php");
		EditText inputFirstname = (EditText) getView().findViewById(R.id.textview_first_name); 
		EditText inputLastname = (EditText) getView().findViewById(R.id.textview_last_name);
		EditText inputStreet = (EditText) getView().findViewById(R.id.textview_address);
		EditText inputCity = (EditText) getView().findViewById(R.id.textview_city);
		EditText inputState = (EditText) getView().findViewById(R.id.textview_state);
		EditText inputZip = (EditText) getView().findViewById(R.id.textview_zip_code);
		
		String firstName = inputFirstname.getText().toString();
		String lastName = inputLastname.getText().toString();
		String street = inputStreet.getText().toString();
		String city = inputCity.getText().toString();
		String state = inputState.getText().toString();
		String zip = inputZip.getText().toString();
		
		updateProfileData.putParameter("firstname", firstName);
		updateProfileData.putParameter("lastname", lastName);
		updateProfileData.putParameter("uid", uid);
		updateProfileData.putParameter("street", street);
		updateProfileData.putParameter("city", city);
		updateProfileData.putParameter("state", state);
		updateProfileData.putParameter("zip", zip);
		
		ServerAsyncTask updateProfile = new ServerAsyncTask();
		try{
			String str = updateProfile.execute(updateProfileData).get();
			Toast.makeText(getActivity().getApplicationContext(),  "Result: " + str, Toast.LENGTH_LONG).show();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void asyncGetProfileData()
	{
		SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
		uid = settings.getString("UID", "Invalid");
		ServerRequestObject getProfileData = new ServerRequestObject();
		getProfileData.setUrl("http://172.9.69.232/wheelshare/profile.php?id=" + uid);
		ServerAsyncTask getProfile = new ServerAsyncTask();
		try{
			String str = getProfile.execute(getProfileData).get();
			if(str == null || str == ""){
				Toast.makeText(getActivity(), "Unavailable Network", Toast.LENGTH_LONG).show();
			}
			else{
				String[] userdataLines = str.split(",");
				
				//setContentView(R.layout.activity_profile);
				
				EditText inputFirstname = (EditText) getView().findViewById(R.id.textview_first_name); 
				EditText inputLastname = (EditText) getView().findViewById(R.id.textview_last_name);
				EditText inputStreet = (EditText) getView().findViewById(R.id.textview_address);
				EditText inputCity = (EditText) getView().findViewById(R.id.textview_city);
				EditText inputState = (EditText) getView().findViewById(R.id.textview_state);
				EditText inputZip = (EditText) getView().findViewById(R.id.textview_zip_code);
				
				inputFirstname.setText(userdataLines[2]);
				inputLastname.setText(userdataLines[3]);
				inputStreet.setText(userdataLines[4]);
				inputCity.setText(userdataLines[5]);
				inputState.setText(userdataLines[6]);
				inputZip.setText(userdataLines[7]);
			}
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
