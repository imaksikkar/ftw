package com.indigohate;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;


import com.indigohate.ServerRequestObject;




public class ServerAsyncTask extends AsyncTask<ServerRequestObject, Integer, String> {
    

    protected void onProgressUpdate(Integer... progress) {
        
    }

    protected void onPostExecute(String result) {
        //return result;
    }

	@Override
	protected String doInBackground(ServerRequestObject... params) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(params[0].getUrl());
		String result = null;
	try{
		httppost.setEntity(new UrlEncodedFormEntity(params[0].getParameters()));	
        // Execute HTTP Post Request
        HttpResponse response = httpclient.execute(httppost);
        HttpEntity httpEntity = response.getEntity();
		InputStream is = httpEntity.getContent(); 
		String str = convertStreamToString(is);
		result = str;
		
	} catch (ClientProtocolException e) {
    	e.printStackTrace();
        
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		return result;
	}
	
	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
}
