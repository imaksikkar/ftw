package com.indigohate;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.v4.app.FragmentManager;


public class UserProfileActivity extends FragmentActivity {

	private final String TAG = "UserProfileActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);
	}
/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_profile, menu);
		return true;
	}*/

	public void saveChangesClick(View view)
	{
		FragmentManager fm = getSupportFragmentManager();
		//updateContact();
		switch(view.getId()){
			case(R.id.profile_tab_save_click):
				ProfileTab profTab = (ProfileTab)fm.findFragmentById(R.id.prof_tab_1);
				profTab.asyncUpdateContact();
				break;
			case(R.id.btn_save_pmt_info):
				PaymentInfoTab payTab = (PaymentInfoTab)fm.findFragmentById(R.id.prof_tab_2);
				payTab.asyncUpdatePaymentInfo();
				break;
			default:
				Log.d(TAG,"uh oh.");
		}
	}
}
