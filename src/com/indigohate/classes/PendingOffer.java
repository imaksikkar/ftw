package com.indigohate.classes;

import android.util.Log;

public class PendingOffer implements PendingRide{
	public String rideId,status, date, time , startLocation, destination, price, seatsLeft;
	public PendingOffer(String rideId, String status, String startLocation, String destination, String date, String time, String price, String seatsLeft){
		this.rideId = rideId;
		this.status = status;
		this.date = date;
		this.time = time;
		this.startLocation = startLocation;
		this.destination = destination;
		this.price = price;
		this.seatsLeft = seatsLeft;
	}
	public String getStatus(){
		return status;
	}
	
	public String getDate(){
		return date;
	}
	
	public String getTime(){
		return time;
	}
	public String getStartLocation(){
		return startLocation;
	}
	public String getDestination(){
		return destination;
	}
	public static PendingOffer stringToOffer(String[] result){
		if(result.length == 9){
			for(String data:result){
				/*if(data==""){
					Log.d("UH", "blank is the cause");
					return null;
				}*/
				Log.d("Make", data);
			}
			return new PendingOffer(result[0],result[1],result[2],result[3],result[4],result[5],result[6],result[7]);
		
		}
		else{
			Log.d("UH", "length is the cause. length is " + result.length);
			for(String data:result){
				Log.d("UHR","data is " + data);
			}
			return null;
		}
	}
}
