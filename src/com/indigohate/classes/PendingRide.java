package com.indigohate.classes;

public interface PendingRide {
	String getStatus();
	String getDate();
	String getTime();
	String getStartLocation();
	String getDestination();
}
