package com.indigohate.classes;

public class RideOffer {
	public String rideId;
	public String firstName;
	public String lastName;
	public String date;
	public String time;
	public String passengerCapacity;
	public String cost;
	public String rating;
	
	public RideOffer(String rideId, String firstName, String lastName, String date, String time, String pass, String cost, String rating){
		this.rideId = rideId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.date = date;
		this.time=time;
		this.passengerCapacity = pass;
		this.cost= cost;
		this.rating = rating;
	}
	
	public static RideOffer parseString(String results){
		try{
			String[] offerStringData = results.split(",");
			String rId, fName,lName,offerDate,startTime,passengersLeft,price,ratingValue;
			if(offerStringData.length > 10){
				rId = offerStringData[0];
				fName = offerStringData[1];
				lName = offerStringData[2];
				offerDate = offerStringData[7];
				startTime = offerStringData[8];
				passengersLeft = offerStringData[9];
				ratingValue = offerStringData[4];
				price = offerStringData[10];
				return new RideOffer(rId,fName,lName,offerDate,startTime,passengersLeft,price,ratingValue);
			}
			else{
				return null;
			}

		}catch(NullPointerException e){
			return null;
		}
	}
}
