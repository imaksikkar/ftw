package com.indigohate.classes;



import com.indigohate.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



public class RideHistoryAdapter extends ArrayAdapter<RideTransaction> {
	Context context;
	int layoutResourceId;
	RideTransaction data[] = null;
	
	public RideHistoryAdapter(Context context, int layoutResourceId, RideTransaction[] data)
	{
		super( context, layoutResourceId, data);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.data = data;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		RideTransactionHolder holder = null;
		
		if(row == null)
		{
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			
			holder = new RideTransactionHolder();
			holder.txtTime = (TextView)row.findViewById(R.id.txtRideHistoryTime);
			holder.txtPrice = (TextView)row.findViewById(R.id.txtRideHistoryPrice);
			holder.txtPassengerCount = (TextView)row.findViewById(R.id.txtRideHistoryPassengers);
			holder.txtStartLocation = (TextView)row.findViewById(R.id.txtRideHistoryStartLocation);
			holder.txtEndLocation = (TextView)row.findViewById(R.id.txtRideHistoryDestination);
			holder.txtDate = (TextView)row.findViewById(R.id.txtRideHistoryDate);
			row.setTag(holder);
		}
		else
		{
			holder = (RideTransactionHolder)row.getTag();
		}
		
		RideTransaction rideTransaction = data[position];
		holder.txtPrice.setText(rideTransaction.price);
		holder.txtPassengerCount.setText(rideTransaction.passengerCount);
		holder.txtStartLocation.setText(rideTransaction.startLocation);
		holder.txtEndLocation.setText(rideTransaction.destination);
		holder.txtDate.setText(rideTransaction.date);
		holder.txtTime.setText(rideTransaction.time);
		
		
		return row;
	}
	
	static class RideTransactionHolder
	{
		TextView txtTime;
		TextView txtPrice;
		TextView txtPassengerCount;
		TextView txtStartLocation;
		TextView txtEndLocation;
		TextView txtDate;
	}
}
