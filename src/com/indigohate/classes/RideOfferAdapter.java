package com.indigohate.classes;


import com.indigohate.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

public class RideOfferAdapter extends ArrayAdapter<RideOffer>{
	Context context;
	int layoutResourceId;
	RideOffer data[] = null;
	
	public RideOfferAdapter(Context context, int layoutResourceId, RideOffer[] data)
	{
		super(context,layoutResourceId,data);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.data = data;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		RideHolder holder = null;
		
		if(row == null)
		{
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			
			holder = new RideHolder();
			holder.firstName = (TextView)row.findViewById(R.id.offerFirstName);
			holder.lastName = (TextView)row.findViewById(R.id.offerLastName);
			holder.rating = (RatingBar)row.findViewById(R.id.offerRatingBar);
			holder.date = (TextView)row.findViewById(R.id.offerDay);
			holder.time = (TextView)row.findViewById(R.id.offerTime);
			holder.cost = (TextView)row.findViewById(R.id.offerPrice);
			holder.passengerCapacity = (TextView)row.findViewById(R.id.offerPassengers);
			row.setTag(holder);
		}
		else
		{
			holder = (RideHolder)row.getTag();
		}
		
		RideOffer ride = data[position];
		holder.firstName.setText(ride.firstName);
		holder.lastName.setText(ride.lastName);
		holder.date.setText(ride.date);
		holder.time.setText(ride.time);
		holder.cost.setText(ride.cost);
		holder.passengerCapacity.setText(ride.passengerCapacity);
		holder.rating.setRating(Float.parseFloat(ride.rating));
		return row;
		
	}
	
	public static class RideHolder
	{
		TextView firstName;
		TextView lastName;
		TextView date;
		TextView time;
		TextView cost;
		TextView passengerCapacity;
		RatingBar rating;
	}
}
