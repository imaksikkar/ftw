package com.indigohate.classes;

import com.indigohate.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

public class RideRequestAdapter extends ArrayAdapter<RideRequestObject>{
	Context context;
	int layoutResourceId;
	RideRequestObject data[] = null;
	
	public RideRequestAdapter(Context context, int layoutResourceId, RideRequestObject[] data){
		super(context,layoutResourceId,data);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.data = data;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		RideRequestHolder holder = null;
		
		if(row == null){
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			holder = new RideRequestHolder();
			holder.firstName = (TextView)row.findViewById(R.id.txtRequestFirstName);
			holder.lastName = (TextView)row.findViewById(R.id.txtRequestLastName);
			holder.date = (TextView)row.findViewById(R.id.txtRequestDate);
			holder.time = (TextView)row.findViewById(R.id.txtRequestTime);
			holder.rating = (RatingBar)row.findViewById(R.id.requestRatingBar);
			row.setTag(holder);
			
		}
		else{
			holder = (RideRequestHolder)row.getTag();
		}
		
		RideRequestObject ride = data[position];
		holder.firstName.setText(ride.firstName);
		holder.lastName.setText(ride.lastName);
		holder.date.setText(ride.date);
		holder.time.setText(ride.time);
		holder.rating.setRating(Float.parseFloat(ride.rating));
		return row;
	}
	
	public static class RideRequestHolder{
		TextView firstName;
		TextView lastName;
		TextView date;
		TextView time;
		RatingBar rating;
	}

}
