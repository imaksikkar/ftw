package com.indigohate.classes;

public class RideTransaction {
	public String rideId,startLocation,destination,date,time,passengerCount,price;
	
	public RideTransaction(String rideId, String startLocation,String destination,String date,String time,String passengerCount ,String price){
		this.rideId = rideId;
		this.startLocation= startLocation;
		this.destination = destination;
		this.date = date;
		this.time = time;
		this.passengerCount = passengerCount;
		this.price = price;
	}
	
	public static RideTransaction stringToObject(String results){
		try{
			String[] historyStringData = results.split(",");
			String rId,sLoc,dest,startTime,historyDate,passCount,cost;
			if(historyStringData.length == 7){
				rId = historyStringData[0];
				sLoc = historyStringData[1];
				dest = historyStringData[2];
				startTime = historyStringData[3];
				historyDate = historyStringData[4];
				passCount = historyStringData[5];
				cost = historyStringData[6];
				return new RideTransaction(rId,sLoc,dest,historyDate,startTime,passCount,cost);
			}
			else{
				return null;
			}
		}catch(NullPointerException e){
			return null;
		}
	}
}
