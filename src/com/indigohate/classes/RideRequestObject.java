package com.indigohate.classes;

public class RideRequestObject {
	public String rideId, firstName,lastName,date,time,rating;
	public RideRequestObject(String rideId, String firstName, String lastName, String date, String time, String rating){
		this.rideId = rideId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.date = date;
		this.time = time;
		this.rating = rating;
	}
	
	public static RideRequestObject stringtoRide(String results){
		try{
			String[] requestStringData = results.split(",");
			String rId,fName,lName,requestDate,startTime,ratingValue;
			if(requestStringData.length > 5){
				rId = requestStringData[0];
				fName = requestStringData[1];
				lName = requestStringData[2];
				requestDate = requestStringData[3];
				startTime = requestStringData[4];
				ratingValue = requestStringData[5];
				return new RideRequestObject(rId,fName,lName,requestDate,startTime,ratingValue);
			}
			else{
				return null;
			}
		}catch(NullPointerException e){
			return null;
		}
	}
}
