package com.indigohate.classes;

import android.content.Context;
import android.widget.Toast;

public class InputCheck {
	public static boolean isStringInputValid(Context context, String... inputs){
		for(String input:inputs){
			if(input==null || input.length()==0 || input.contains(" ")){
				Toast.makeText(context, "An input field is empty or invalid", Toast.LENGTH_LONG).show();
				return false;
			}
		}
		return true;
	}
	//may not be necessary
	public static boolean isValidNumber(Context context, String... inputs){
		for(String input:inputs){
			try{
				Float.parseFloat(input);
			}catch(NumberFormatException e){
				Toast.makeText(context, "You've entered an invalid number",  Toast.LENGTH_LONG).show();
				return false;
			}
		}
		return true;
	}
	
	//Contains no numbers
	public static boolean isValidString(Context context, String... inputs){
		for(String input:inputs){
			if(input==null || input.length()==0 || input.matches(".*\\d.*")){
				Toast.makeText(context, "You've entered an invalid input",  Toast.LENGTH_LONG).show();
				return false;
			}
		}
		return true;
	}
	
	public static boolean isEmailValid(Context context, String email){
		if(email.contains("@") && email.contains(".")){
			return true;
		}
		Toast.makeText(context, "Please enter a valid email",  Toast.LENGTH_LONG).show();
		return false;
	}
	
	public static boolean isCreditCardValid(Context context, String creditCardNumber){
		if(creditCardNumber.length() != 16){
			Toast.makeText(context, "Invalid credit card number", Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
			
	}
}
