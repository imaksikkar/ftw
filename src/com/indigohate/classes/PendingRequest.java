package com.indigohate.classes;

import android.util.Log;

public class PendingRequest implements PendingRide{
	public String rideId,status, date, time , startLocation, destination;
	public PendingRequest(String rideId, String status, String date, String time, String startLocation, String destination){
		this.rideId = rideId;
		this.status = status;
		this.date = date;
		this.time = time;
		this.startLocation = startLocation;
		this.destination = destination;
	}
	public String getStatus(){
		return status;
	}
	
	public String getDate(){
		return date;
	}
	
	public String getTime(){
		return time;
	}
	public String getStartLocation(){
		return startLocation;
	}
	public String getDestination(){
		return destination;
	}
	public static PendingRequest stringToRequest(String[] result){
		if(result.length == 6){
			for(String data:result){
				/*if (data == ""){
					Log.d("UHR","blank is the cause");
					return null;
				}*/
				Log.d("make",data);
			}
			return new PendingRequest(result[0],result[1],result[4],result[5],result[2],result[3]);
		}
		Log.d("UHR", "length is the cause. " + result.length);
		for(String data:result){
			Log.d("UHR","data is " + data);
		}
		return null;
	}
}
