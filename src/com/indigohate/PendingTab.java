package com.indigohate;

import java.util.ArrayList;

import com.indigohate.ConfirmedTab.ConfirmedArrayAdapter;
import com.indigohate.ConfirmedTab.ConfirmedArrayAdapter.ConfirmedRideHolder;
import com.indigohate.classes.PendingRequest;
import com.indigohate.classes.PendingRide;
import com.indigohate.classes.PendingOffer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("ValidFragment")
public class PendingTab extends Fragment{
	private static final String TAG = "PendingList";

	private String mTag;
	//private MyAdapter mAdapter;
	private ArrayList<String> mItems;
	private LayoutInflater mInflater;
	private int mTotal;
	private int mPosition;
	PendingRide[] data;
	public PendingTab(){
		
	}
	
	public PendingTab(String tag){
		this.mTag = tag;
		Log.d(TAG,"constructor");
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View V = inflater.inflate(R.layout.pending_fragment, container, false);
        Log.d(TAG, "created");
          return V;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d(TAG,"wtf");
		// this is really important in order to save the state across screen
		// configuration changes for example
		setRetainInstance(true);

		mInflater = LayoutInflater.from(getActivity());
		
		FragmentManager fm = getFragmentManager();
		if(getActivity() != null){
			Log.d(TAG,"get activity is not null! yay");
			AllMyRidesFragmentActivity tabHost = (AllMyRidesFragmentActivity)getActivity();
			data = tabHost.getPendingRides();
			mTotal = data.length;
			ListView listView = (ListView)getView().findViewById(R.id.pendingListView);
			Log.d(TAG,"right before adapter");
			PendingArrayAdapter cAdapter = new PendingArrayAdapter(getActivity(),
					R.layout.pending_listview_item,
					data);
			Log.d(TAG,"just made the adapter");

			listView.setOnItemClickListener(new OnItemClickListener()
				{
					@Override 
					public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
				    { 
						Intent intent = new Intent(getActivity(), ExistingOfferOrRequestActivity.class);
						Bundle extras = new Bundle();
						if(data[position].getStatus().contains("Offer")){
							PendingOffer pendingOffer = (PendingOffer)data[position];
							String rideId = pendingOffer.rideId;
							String rideType = pendingOffer.status;
							String owner = pendingOffer.rideId;
							String date = pendingOffer.date;
							String from = pendingOffer.startLocation;
							String time = pendingOffer.time;
							String to = pendingOffer.destination;
							String cost = pendingOffer.price;
							String seatsLeft = pendingOffer.seatsLeft;
							
							
							extras.putString("rideId",rideId);
							extras.putString("rideType", rideType);
							extras.putString("owner", owner);
							extras.putString("date", date);
							extras.putString("time", time);
							extras.putString("startLocation", to);
							extras.putString("destination", from);
							extras.putString("price", cost);
							extras.putString("seatsLeft", seatsLeft);
							
							
						}
						else{
							PendingRequest pendingRequest = (PendingRequest)data[position];
							String rideId = pendingRequest.rideId;
							String date = pendingRequest.getDate();
							String rideType = pendingRequest.status;
							String time = pendingRequest.getTime();
							String startLocation = pendingRequest.getStartLocation();
							String destination = pendingRequest.getDestination();
							extras.putString("rideIf", rideId);
							extras.putString("rideType", rideType);
							extras.putString("date", date);
							extras.putString("time", time);
							extras.putString("startLocation", startLocation);
							extras.putString("destination", destination);
							
						}
						//Navigate to the ride details here
				        Toast.makeText(getActivity().getApplicationContext(), "" + position, Toast.LENGTH_LONG).show();
				        intent.putExtras(extras);
				        getActivity().startActivity(intent);
				    }
				}
			);
			listView.setEmptyView(getView().findViewById(R.id.pendingEmptyView));
			/*listView.setOnItemClickListener(new OnItemClickListener()
				{
					@Override 
					public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
				    { 
						//Navigate to the ride details here
						String data = ((TextView)arg1.findViewById(R.id.txtPendingStatus)).getText().toString();
				        Toast.makeText(getActivity().getApplicationContext(), "" + position + " " + data, Toast.LENGTH_LONG).show();
				    }
				}
			);*/
			listView.setAdapter(cAdapter);
			Log.d(TAG,"just set the adapter");
		}
		else{
			Log.d(TAG,"shit..");
		}
	}
	
	public class PendingArrayAdapter extends ArrayAdapter<PendingRide>{
		Context context;
		int layoutResourceId;
		PendingRide data[] = null;
		private String TAG = "pendingadapter";
		
		public PendingArrayAdapter(Context context, int layoutResourceId, PendingRide[] data){
			super(context,layoutResourceId,data);
			this.context = context;
			this.layoutResourceId = layoutResourceId;
			this.data = data;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent){
			
			View row = convertView;
			PendingRideHolder holder = null;
			Log.d(TAG, "in getview");
			if(row ==null){
				Log.d(TAG,"in in");
				LayoutInflater inflater = ((Activity)context).getLayoutInflater();
				Log.d(TAG,"after activity cast");
				row = inflater.inflate(layoutResourceId, parent, false);
				Log.d(TAG,"inflating row");
				holder = new PendingRideHolder();
				Log.d(TAG,"before textview cast");
				holder.textStatus = (TextView)row.findViewById(R.id.txtPendingStatus);
				holder.textDate= (TextView)row.findViewById(R.id.txtPendingDate);
				holder.textTime = (TextView)row.findViewById(R.id.txtPendingTime);
				holder.textStartLocation = (TextView)row.findViewById(R.id.txtStartLocPending);
				holder.textDestination = (TextView)row.findViewById(R.id.txtDestinationPending);
				Log.d(TAG,"before settag");
				row.setTag(holder);
				Log.d(TAG,"after settag");
			}
			else
			{
				holder = (PendingRideHolder)row.getTag();
			}
			Log.d(TAG,"before assigning data");
			PendingRide result = data[position];
			Log.d(TAG,"before setstatus id");
			holder.textStatus.setText(result.getStatus());
			Log.d(TAG,"before setdate");
			holder.textDate.setText(result.getDate());
			Log.d(TAG,"before settime");
			holder.textTime.setText(result.getTime());
			Log.d(TAG,"before setstart");
			holder.textStartLocation.setText(result.getStartLocation());
			Log.d(TAG,"before setdest");
			holder.textDestination.setText(result.getDestination());
			Log.d(TAG,"before return row");
			return row;
		}
		class PendingRideHolder{
			TextView textStatus;
			TextView textDate;
			TextView textTime;
			TextView textStartLocation;
			TextView textDestination;
		}
	}


}
