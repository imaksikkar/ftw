package com.indigohate;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.Menu;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.indigohate.ServerRequestObject;
import com.indigohate.ServerAsyncTask;
import com.indigohate.classes.InputCheck;
import com.indigohate.messageEndpoint.MessageEndpoint;

public class LoginGCMActivity extends Activity {

	Button btnLogin;
	Button btnLinkToRegister;
	EditText inputEmail;
	EditText inputPassword;
	TextView loginErrorMsg;

	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String EXTRA_USERNAME = "com.indigohate.extra.USERNAME";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	  enum State {
		    REGISTERED, REGISTERING, UNREGISTERED, UNREGISTERING
		  }

		  private State curState = State.UNREGISTERED;
		  private OnTouchListener registerListener = null;
		  private OnTouchListener unregisterListener = null;
		  private MessageEndpoint messageEndpoint = null;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_gcm);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login_gcm, menu);
		return true;
	}
	public void onClickRegisterButton(View view){
		Intent i = new Intent(getApplicationContext(), UserRegister.class);
		startActivity(i);
		finish();
	}
	
	public void onClickCantLoginButton(View view){
		Intent intent = new Intent(this,LostPasswordActivity.class);
		startActivity(intent);
	}
	
	public void onClickLoginButton(View view){
		String result = asyncLogin();
		//doLogin();
		if(result.contains("Authorized")){
		
		try {
            GCMIntentService.register(getApplicationContext());
          } catch (Exception e) {
        	  e.printStackTrace();
          }
		Intent i = new Intent(this, HomeActivity.class);
		
		
		}	
	}
	
	private String asyncLogin(){
		
		//ServerAsyncTask loginTask = new ServerAsyncTask();
		
		inputEmail = (EditText) findViewById(R.id.login_username);
		inputPassword = (EditText) findViewById(R.id.login_password);
		loginErrorMsg = (TextView) findViewById(R.id.login_error_message);
		String result = null;
		String email = inputEmail.getText().toString();
		String password = inputPassword.getText().toString();
		if(InputCheck.isStringInputValid(this, email, password)){
			ServerRequestObject loginData = new ServerRequestObject();
			loginData.setUrl("http://172.9.69.232/wheelshare/postlogin.php");
			loginData.putParameter("username", email);
			loginData.putParameter("password", password);
			
			ServerAsyncTask login = new ServerAsyncTask();
			try {
				String str = login.execute(loginData).get();
				//Toast.makeText(this, "Result: " + str, Toast.LENGTH_SHORT).show();
				if(str.contains("Authorized")) {	
				// user successfully logged in 
				// start up homepage/dashboard activity
				//get UID to pass on.  UID is concatenated to Authorized
				String str2 = str.replaceAll("\\D+","");
				result = "Authorized";
				//push the UID to SharedPreferences for persistence
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("UID", str2);
				editor.putString("USERNAME", email);
				// Commit the edits!
				editor.commit();
					
					//Intent i = new Intent(this, HomeActivity.class);
					//Intent i = new Intent(this, LoadGCMRegisterActivity.class);
					//Intent i = new Intent(this, RegisterActivity.class);
				    //startActivity(i);
					
					//finish(); 	// close Login screen
				
				} else {
					//Toast.makeText(this,  "Login Result: " + str, Toast.LENGTH_LONG).show();
			        //Error in login, server returned something other than Authorized
					loginErrorMsg.setText("Invalid Username/Password");
					result = "Invalid";
			
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
		
		
		//new ServerAsyncTask().execute(loginData);
	}
	

	
public static String convertStreamToString(java.io.InputStream is) {
	    //utility function for parsing the HTTP response 
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
}


/*
public class EndpointsTask extends AsyncTask<UserData, Void, Void> {
    @Override
	protected Void doInBackground(UserData... userdata) {

      Userdataendpoint.Builder endpointBuilder = new Userdataendpoint.Builder(
          AndroidHttp.newCompatibleTransport(),
          new JacksonFactory(),
          new HttpRequestInitializer() {
          public void initialize(HttpRequest httpRequest) { }
          });
  endpointBuilder.setApplicationName("electric-sheep160");
  Userdataendpoint endpoint = CloudEndpointUtils.updateBuilder(
  endpointBuilder).build();
  try {
      //UserData user = new UserData().setMessage("test");
      
      //String userID = new Date().toString();
      //user.setId(userID);

      //UserData result = 
	  //endpoint.insertSQLUserData(userdata[0]).execute();
      //endpoint.insertUserData(userdata[0]).execute();
      endpoint.getUserData(userdata[0]).execute();
      } catch (IOException e) {
    e.printStackTrace();
  }
      return null;
    }
}
*/

}


