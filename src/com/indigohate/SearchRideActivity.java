package com.indigohate;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;


import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.EditText;


public class SearchRideActivity extends Activity {
	public String uid;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_ride);
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

		uid = settings.getString("UID", "Invalid");
		
		
	}

	public void onClickSubmitReview(View view){
				
		EditText startLocationInput = (EditText) findViewById(R.id.search_start_location);
		EditText endLocationInput = (EditText) findViewById(R.id.search_end_location);
		EditText rideDateInput = (EditText) findViewById(R.id.search_date_picker);
		EditText rideTimeInput = (EditText) findViewById(R.id.search_start_time_picker);
				
		String startLocation = startLocationInput.getText().toString();
		String endLocation = endLocationInput.getText().toString();
		String rideDate = rideDateInput.getText().toString();
		String rideTime = rideTimeInput.getText().toString();
		
		
		Intent intent = new Intent(this, SearchForOffersActivity.class);
		
		Bundle extras = intent.getExtras();
		extras.putString("startLocation", startLocation);
		extras.putString("endLocation", endLocation);
		extras.putString("rideDate", rideDate);
		extras.putString("rideTime", rideTime);
		
				
		startActivity(intent);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_ride, menu);
		return true;
	}

	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
}
