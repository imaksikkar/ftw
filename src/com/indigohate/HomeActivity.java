package com.indigohate;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import com.indigohate.messageEndpoint.MessageEndpoint;
import com.indigohate.messageEndpoint.model.CollectionResponseMessageData;
import com.indigohate.messageEndpoint.model.MessageData;
import com.google.android.gcm.GCMRegistrar;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.jackson.JacksonFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import static com.indigohate.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.indigohate.CommonUtilities.EXTRA_MESSAGE;
import static com.indigohate.CommonUtilities.SENDER_ID;
import static com.indigohate.CommonUtilities.SERVER_URL;

import com.indigohate.ServerUtilities;

public class HomeActivity extends Activity {
	public String uid;
	
	public String UserName;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	private MessageEndpoint messageEndpoint = null;
//	AsyncTask<Void, Void, Void> mRegisterTask;

	AsyncTask<Void, Void, Void> mRegisterTask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//uid = savedInstanceState.getString(EXTRA_UID);
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		uid = settings.getString("UID", "Invalid");
		UserName = settings.getString("USERNAME", "Invalid");
		setContentView(R.layout.activity_home);
		//Toast.makeText(getApplicationContext(), uid, Toast.LENGTH_LONG).show();
		
		TextView userNameDisplay = (TextView) findViewById(R.id.home_username);
		userNameDisplay.setText(UserName);
		
		MessageEndpoint.Builder endpointBuilder = new MessageEndpoint.Builder(
		        AndroidHttp.newCompatibleTransport(),
		        new JacksonFactory(),
		        new HttpRequestInitializer() {
		          public void initialize(HttpRequest httpRequest) { }
		        });
			endpointBuilder.setApplicationName("electric-sheep160");
		    messageEndpoint = CloudEndpointUtils.updateBuilder(endpointBuilder).build();
		    GCMRegistrar.checkDevice(this);
	        // Make sure the manifest was properly set - comment out this line
	        // while developing the app, then uncomment it when it's ready.
	        GCMRegistrar.checkManifest(this);
	        
	        //mDisplay = (TextView) findViewById(R.id.display);
	        registerReceiver(mHandleMessageReceiver,
	                new IntentFilter(DISPLAY_MESSAGE_ACTION));
	        final String regId = GCMRegistrar.getRegistrationId(this);
	        if (regId.equals("")) {
	            // Automatically registers application on startup.
	            GCMRegistrar.register(this, SENDER_ID);
	        } else {
	            // Device is already registered on GCM, check server.
	            if (GCMRegistrar.isRegisteredOnServer(this)) {
	                // Skips registration.
	                //mDisplay.append(getString(R.string.already_registered) + "\n");
	            } else {
	                // Try to register again, but not in the UI thread.
	                // It's also necessary to cancel the thread onDestroy(),
	                // hence the use of AsyncTask instead of a raw thread.
	                final Context context = this;
	                mRegisterTask = new AsyncTask<Void, Void, Void>() {

	                    @Override
	                    protected Void doInBackground(Void... params) {
	                        boolean registered =
	                                ServerUtilities.register(context, regId);
	                        // At this point all attempts to register with the app
	                        // server failed, so we need to unregister the device
	                        // from GCM - the app will try to register again when
	                        // it is restarted. Note that GCM will send an
	                        // unregistered callback upon completion, but
	                        // GCMIntentService.onUnregistered() will ignore it.
	                        if (!registered) {
	                            GCMRegistrar.unregister(context);
	                        }
	                        return null;
	                    }

	                    @Override
	                    protected void onPostExecute(Void result) {
	                        mRegisterTask = null;
	                    }

	                };
	                mRegisterTask.execute(null, null, null);
	            }
	        }
	        ServerRequestObject regIDpackage = new ServerRequestObject();
	        ServerAsyncTask sendRegID = new ServerAsyncTask();
	        regIDpackage.setUrl("http://172.9.69.232/wheelshare/setregid.php");
	        regIDpackage.putParameter("regid", regId);
	        regIDpackage.putParameter("uid", uid);
	        sendRegID.execute(regIDpackage);
	}

	@Override
	  protected void onNewIntent(Intent intent) {
	    super.onNewIntent(intent);
	    new QueryMessagesTask(this, messageEndpoint).execute();
	    
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}
	
	public void onClickAccountButton(View view){
		Intent intent = new Intent(this, UserProfileActivity.class);
		startActivity(intent);
		
	}

	public void onClickCalenderButton(View view){
		Intent intent = new Intent(this, AllMyRidesFragmentActivity.class);
		startActivity(intent);
	}
	
	public void onClickRideRequestButton(View view){
		Intent intent = new Intent(this, RideRequest.class);
		startActivity(intent);
	}
	
	public void onClickRideOfferButton(View view){
		Intent intent = new Intent(this, RideOfferActivity.class);
		startActivity(intent);
	}
	
/*	public void onClickStartRideButton(View view){
		Intent intent = new Intent(this, StartRideActivity.class);
		startActivity(intent);
	}
*/	
	public void onClickAllRidesButton(View view){
		Intent intent = new Intent(this, AllMyRidesFragmentActivity.class);
		startActivity(intent);
	}
	
	public void onLogoutButtonClick(View view){
		finish();
	}
	private class QueryMessagesTask 
    extends AsyncTask<Void, Void, CollectionResponseMessageData> {
	  Exception exceptionThrown = null;
	  Activity activity;
	  MessageEndpoint messageEndpoint;
	
	  public QueryMessagesTask(Activity activity, MessageEndpoint messageEndpoint) {
	    this.activity = activity;
	    this.messageEndpoint = messageEndpoint;
  }
  
  @Override
  protected CollectionResponseMessageData doInBackground(Void... params) {
    try {
      CollectionResponseMessageData messages = 
          messageEndpoint.listMessages().setLimit(1).execute();
      return messages;
    } catch (IOException e) {
      exceptionThrown = e;
      return null;
      //Handle exception in PostExecute
    }            
  }
  
  protected void onPostExecute(CollectionResponseMessageData messages) {
    // Check if exception was thrown
    if (exceptionThrown != null) {
      Log.e(RegisterActivity.class.getName(), 
          "Exception when listing Messages", exceptionThrown);
      showDialog("Failed to retrieve the last 5 messages from " +
      		"the endpoint at " + messageEndpoint.getBaseUrl() +
      		", check log for details", "", "");
    }
    else {
	 String messageData = null;
	 String rideId;
	 String passengerId;
	 String passengerUsername;
	  for(MessageData message : messages.getItems()) {
	    //messageData += message.getMessage() + "\n";
		  
		  String[] messageItems = message.getMessage().split(",");
		  if(messageItems[0].equals(uid)){
			  Log.d("Home Activity", "messageitems[0] == uid");
			  rideId = messageItems[1];
			  passengerId = messageItems[2];
			 // passengerUsername = messageItems[3];
			  
			  Log.d("Home Activity", "uid : "+uid);
			  Log.d("Home Activity", "rideid : "+rideId);
			  Log.d("Home Activity", "passengerid : "+passengerId);
			  Log.d("Home Activity", "messsage:getMessage() : "+message.getMessage());

			  showDialog("A user has requested to join your ride #"
					  +rideId+". Would you like to accept?", rideId, passengerId);
		  }
		  else{
			  Log.d("Home Activity", "messageitems[0] != uid");
		  }
      }
      //showDialog(messageData);
    }
  }   
}
	
	  private void showDialog(String message, final String rideId, final String passengerId) {
		    new AlertDialog.Builder(this)
		        .setMessage(message)
		        .setPositiveButton("Accept",
		            new DialogInterface.OnClickListener() {
		              public void onClick(DialogInterface dialog, int id) {
		            	asyncConfirmPassenger(rideId, passengerId);
		                dialog.dismiss();
		              }
		            })//.show();
		    .setNegativeButton("Reject",
		            new DialogInterface.OnClickListener() {
		              public void onClick(DialogInterface dialog, int id) {
		                dialog.dismiss();
		              }
		            }).show();
		  }
	  
	  
	  private void asyncConfirmPassenger(String rideId, String passengerId)
	    {
	    
	    	ServerRequestObject startRide = new ServerRequestObject();
	    	startRide.setUrl("http://172.9.69.232/wheelshare/confirmpassenger.php");

	    	startRide.putParameter("uid", uid);
	    	startRide.putParameter("rideid", rideId);
	    	startRide.putParameter("passengerid", passengerId);
	    	
	    	ServerAsyncTask requestRide = new ServerAsyncTask();
	    	try{
	    		String str = requestRide.execute(startRide).get();
		    	Toast.makeText(this,  "Result Code: " + str, Toast.LENGTH_LONG).show();
	    	
	    	}catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	  private final BroadcastReceiver mHandleMessageReceiver =
	            new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
	            showDialog(newMessage, "test", "test2");
	            //mDisplay.append(newMessage + "\n");
	        }
	    };
}
