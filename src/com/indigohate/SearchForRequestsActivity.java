package com.indigohate;


import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


import com.indigohate.SearchResultsListAdapter;
import com.indigohate.classes.RideRequestAdapter;
import com.indigohate.classes.RideRequestObject;


import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

// Search results displayed when user creates new ride offer
public class SearchForRequestsActivity extends ListActivity {
	
	public String uid;
	public String rideId; 
	public String ridedataResult;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
	@SuppressWarnings("unused")
    private Callbacks mCallbacks = sRideCallbacks;
    private int mActivatedPosition = ListView.INVALID_POSITION;
    private ListView searchResultsList;
    boolean checked = true;
	boolean unchecked = false;
	public ArrayList<RideRequestObject> rideRequests;
	public enum SortType { NONE, SORTBYTIME, SORTBYRATING }
	String fromLocation;
	String toDestination;
	String startdate;
	String starttime;
	
	
    public interface Callbacks {

        public void onItemSelected(String id);
    }
//searchrequests.php
    private static Callbacks sRideCallbacks = new Callbacks(){
    	@Override
    	public void onItemSelected(String id){}
    };
    
    public SearchForRequestsActivity() {
    }

    
    @Override
    public void onCreate(Bundle extras) {
        super.onCreate(extras);
        
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);		
		uid = settings.getString("UID", "Invalid");
		fromLocation = getIntent().getStringExtra("startLocation");
    	toDestination = getIntent().getStringExtra("endLocation");
    	startdate = getIntent().getStringExtra("rideDate");
    	starttime = getIntent().getStringExtra("rideTime");
		setContentView(R.layout.activity_search_for_requests_list);
		asyncSearchRequests();
	}
 
    public void onCLickNoSort(View view){
    	//Make the async call here and reset adapter
    	Toast.makeText(this, "no filter", Toast.LENGTH_SHORT).show();
    }
    
    public void onClickSortTime(View view)
    {
    	Toast.makeText(this, "sort price", Toast.LENGTH_SHORT).show();
    	//Make the async call here and reset adapter
    }
    
    public void onClickSortRating(View view)
    {
    	Toast.makeText(this, "sort rating", Toast.LENGTH_SHORT).show();
    	//Make the async call here and reset adapter


    }
    
   private void asyncUpdateSort(SortType sortType){
	   	rideRequests = new ArrayList<RideRequestObject>();
   		ServerRequestObject rideData = new ServerRequestObject();
   		rideData.setUrl("http://172.9.69.232/wheelshare/searchrequests.php");
   		rideData.putParameter("fromlocation", fromLocation);
    	rideData.putParameter("todestination", toDestination);
    	rideData.putParameter("starttime", starttime);
    	rideData.putParameter("startdate", startdate);
    	if(sortType == SortType.SORTBYRATING){
    		rideData.putParameter("sortbyrating", "Yes");
    	}
    	else if(sortType == SortType.SORTBYTIME){
    		rideData.putParameter("sortbytime", "Yes");
    	}
    	else{
    		
    	}
    	ServerAsyncTask searchRide = new ServerAsyncTask();
    	ArrayList<String> dataLinesList = new ArrayList<String>();
    	try{
    		String str = searchRide.execute(rideData).get();
    		if(str == null || str==""){
    			Toast.makeText(this, "Unavailable Network", Toast.LENGTH_LONG).show();
    			Log.d("offer", "invalid http results");
    		}
    		else{
    			String[] ridedataLines = str.split(";");
    			for(String resultString:ridedataLines){
    				if(resultString.startsWith("EOF"))
    					break;
    				RideRequestObject parsedRequest = RideRequestObject.stringtoRide(resultString);
    				if(parsedRequest == null){
    					Log.d("request", "invalid data");
    				}
    				else{
    					rideRequests.add(parsedRequest);
    				}
    			}
    		}
    		/*String[] ridedataLines = str.split(";");
    		try{
    			
   			 for(int i=0; i < ridedataLines.length; i++){
   				dataLinesList.add(ridedataLines[i]);
   				Log.d("SearchRequests", "dataLinesList["+i+"] = "+ridedataLines[i]);
   			 }
   			
   			}catch(NullPointerException e){
   				e.printStackTrace();
   			}*/
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	searchResultsList = getListView();
    	RideRequestAdapter adapter = new RideRequestAdapter(this,
				R.layout.request_listview_item,
				rideRequests.toArray(new RideRequestObject[rideRequests.size()]));
    	/*
		SearchResultsListAdapter adapter = new SearchResultsListAdapter(this,
				android.R.layout.simple_list_item_1,
				dataLinesList);*/
		searchResultsList.setAdapter(adapter);
   		
   }
    
    
    private void asyncSearchRequests(){
    	rideRequests = new ArrayList<RideRequestObject>();
    	
    	
    	ServerRequestObject rideData = new ServerRequestObject();
    	rideData.setUrl("http://172.9.69.232/wheelshare/searchrequests.php");
    	
    	String fromLocation = getIntent().getStringExtra("startLocation");
    	String toDestination = getIntent().getStringExtra("endLocation");
    	String startdate = getIntent().getStringExtra("rideDate");
    	String starttime = getIntent().getStringExtra("rideTime");
    	rideId = getIntent().getStringExtra("rideId");
    	
  //  	String rideDate = getIntent().getStringExtra("rideDate");
  //  	String rideTime = getIntent().getStringExtra("rideTime");
    	
    	rideData.putParameter("fromlocation", fromLocation);
    	rideData.putParameter("todestination", toDestination);
    	rideData.putParameter("starttime", starttime);
    	rideData.putParameter("startdate", startdate);
    	Log.d("SearchRequests", "from: "+fromLocation);
    	Log.d("SearchRequests", "to: "+toDestination); 
		 
//    	rideData.putParameter("fromdate", rideDate);
//    	rideData.putParameter("starttime", rideTime);
    	ServerAsyncTask searchRide = new ServerAsyncTask();
    	ArrayList<String> dataLinesList = new ArrayList<String>();
    	try{
    		String str = searchRide.execute(rideData).get();
    		if(str == null || str==""){
    			Toast.makeText(this, "Unavailable Network", Toast.LENGTH_LONG).show();
    			Log.d("offer", "invalid http results");
    		}
    		else{
    			String[] ridedataLines = str.split(";");
    			for(String resultString:ridedataLines){
    				if(resultString.startsWith("EOF"))
    					break;
    				RideRequestObject parsedRequest = RideRequestObject.stringtoRide(resultString);
    				if(parsedRequest == null){
    					Log.d("request", "invalid data");
    				}
    				else{
    					rideRequests.add(parsedRequest);
    				}
    			}
    		}
    		/*String[] ridedataLines = str.split(";");
    		try{
    			
   			 for(int i=0; i < ridedataLines.length; i++){
   				dataLinesList.add(ridedataLines[i]);
   				Log.d("SearchRequests", "dataLinesList["+i+"] = "+ridedataLines[i]);
   			 }
   			
   			}catch(NullPointerException e){
   				e.printStackTrace();
   			}*/
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	searchResultsList = getListView();
    	View header = (View)getLayoutInflater().inflate(R.layout.searchrequest_listview_header, null);
		RideRequestAdapter adapter = new RideRequestAdapter(this,
				R.layout.request_listview_item,
				rideRequests.toArray(new RideRequestObject[rideRequests.size()]));
    	searchResultsList.addHeaderView(header);
    	/*
		SearchResultsListAdapter adapter = new SearchResultsListAdapter(this,
				android.R.layout.simple_list_item_1,
				dataLinesList);*/
		searchResultsList.setAdapter(adapter);
    }
    
    
    public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
    }
    
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        if(position == 0){
        	
        }
        else{
        	RideRequestObject rideRequest = rideRequests.get(position-1);
            
            //Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
            
            Intent intent = new Intent(this, RequestDetailsActivity.class);
            Bundle extras = new Bundle();
            extras.putString("ride", rideRequest.rideId);
            extras.putString("rideId", rideId);
            intent.putExtras(extras);
            
            startActivity(intent);
        }
        
        
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    public void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }
    
}
