package com.indigohate;

import java.util.concurrent.ExecutionException;

import com.indigohate.classes.InputCheck;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PasswordResetActivity extends Activity {
	
	public String userdataResult;
	public String profileIdentifier = "profile";
	public String uid;
	public String tempPW;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password_reset);
		uid = getIntent().getStringExtra("UID");
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.password_reset, menu);
		return true;
	}
	
	public void onChangePasswordClicked(View view){
		EditText newPwd = (EditText) findViewById(R.id.new_password);
		EditText reenterPwd = (EditText) findViewById(R.id.reenter_new_password);
		TextView message = (TextView) findViewById(R.id.message);
		
		String newPassword = newPwd.getText().toString();
		String reenterPassword = reenterPwd.getText().toString();
		
		if(!newPassword.equals(reenterPassword)) {
			message.setText("Password does not match");
		}
		else{
			// code to verify that tempPassword matches generated temporary pwd
			// code to change user's password
			boolean check = asyncCheckTempPassword();
			if(check == true){
				Log.d("resetPassword", "temp pw match, updating password");
				asyncUpdatePassword(newPassword);
				Intent intent = new Intent(this, LoginActivity.class);
				startActivity(intent);
			}
			else {
				TextView loginErrorMsg = (TextView) findViewById(R.id.wrong_temp_message);
				loginErrorMsg.setText("Invalid Username/Password");
				Log.d("resetPassword", "failed to match temp pw");
			}
		}
	}

	private void asyncUpdatePassword(String newPW) {
		
		ServerRequestObject updatePasswordData = new ServerRequestObject();
		updatePasswordData.setUrl("http://172.9.69.232/wheelshare/changepassword.php");
		
		updatePasswordData.putParameter("uid", uid);
		updatePasswordData.putParameter("password", newPW);
		
		ServerAsyncTask updatePassword = new ServerAsyncTask();
		try{
			String str = updatePassword.execute(updatePasswordData).get();
			Toast.makeText(this,  "Result: " + str, Toast.LENGTH_LONG).show();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private boolean asyncCheckTempPassword(){
		
		EditText tempPassword = (EditText) findViewById(R.id.input_temp_password);
		tempPW = tempPassword.getText().toString();
		
		String username = asyncGetUsername();
		
		ServerRequestObject loginData = new ServerRequestObject();
		loginData.setUrl("http://172.9.69.232/wheelshare/postlogin.php");
		loginData.putParameter("username", username);
		loginData.putParameter("password", tempPW);
		
		ServerAsyncTask login = new ServerAsyncTask();
		try {
			String str = login.execute(loginData).get();
			Toast.makeText(this, "Result: " + str, Toast.LENGTH_SHORT).show();
			if(str.contains("Authorized")) {
				String str2 = str.replaceAll("\\D+","");
				uid = str2;
				return true;
			} else {
		        //Error in login, server returned something other than Authorized
				return false;
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	private String asyncGetUsername()
    {
            String username = null;
            
            ServerRequestObject getProfileData = new ServerRequestObject();
    		getProfileData.setUrl("http://172.9.69.232/wheelshare/profile.php?id=" + uid);
    		ServerAsyncTask getProfile = new ServerAsyncTask();
    		try{
    			String str = getProfile.execute(getProfileData).get();
    			String[] userdataLines = str.split(",");
    			username = userdataLines[0];
    		
    		}catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ExecutionException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            return username;
    }
	
}