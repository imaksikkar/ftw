package com.indigohate;



import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



public class RideHistoryAdapter extends ArrayAdapter<RideTransaction> {
	Context context;
	int layoutResourceId;
	RideTransaction data[] = null;
	
	public RideHistoryAdapter(Context context, int layoutResourceId, RideTransaction[] data)
	{
		super( context, layoutResourceId, data);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.data = data;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		RideTransactionHolder holder = null;
		
		if(row == null)
		{
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			
			holder = new RideTransactionHolder();
			holder.txtFirstName = (TextView)row.findViewById(R.id.txtFirstName);
			holder.txtLastName = (TextView)row.findViewById(R.id.txtLastName);
			holder.txtPassengers = (TextView)row.findViewById(R.id.txtPassengers);
			holder.txtStartLocation = (TextView)row.findViewById(R.id.txtStartLocation);
			holder.txtEndLocation = (TextView)row.findViewById(R.id.txtEndLocation);
			holder.txtDateTime = (TextView)row.findViewById(R.id.txtDate);
			row.setTag(holder);
		}
		else
		{
			holder = (RideTransactionHolder)row.getTag();
		}
		
		RideTransaction rideTransaction = data[position];
		holder.txtFirstName.setText(rideTransaction.firstName);
		holder.txtLastName.setText(rideTransaction.lastName);
		holder.txtPassengers.setText(rideTransaction.passengers.get(0));
		holder.txtStartLocation.setText(rideTransaction.startLocation);
		holder.txtEndLocation.setText(rideTransaction.endLocation);
		holder.txtDateTime.setText(rideTransaction.dateTime.toString());
		
		return row;
	}
	
	static class RideTransactionHolder
	{
		TextView txtFirstName;
		TextView txtLastName;
		TextView txtPassengers;
		TextView txtStartLocation;
		TextView txtEndLocation;
		TextView txtDateTime;
	}
}
