package com.indigohate;



import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.indigohate.ServerRequestObject;
import com.indigohate.ServerAsyncTask;
import com.indigohate.classes.InputCheck;



public class LoginActivity extends Activity {
	Button btnLogin;
	Button btnLinkToRegister;
	EditText inputEmail;
	EditText inputPassword;
	TextView loginErrorMsg;
	String preset = null;


	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String EXTRA_USERNAME = "com.indigohate.extra.USERNAME";
	public static final String PREFS_NAME = "WheelsharePrefsFile";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	//private UserLoginTask mAuthTask = null;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		//new LoadViewTask().execute();
		
		preset = getIntent().getStringExtra("USERNAME");
		if(preset != null){
			inputEmail = (EditText) findViewById(R.id.email);
			inputEmail.setText(preset);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	public void onClickRegisterButton(View view){
		Intent i = new Intent(getApplicationContext(), UserRegister.class);
		startActivity(i);
		finish();
	}
	
	public void onClickCantLoginButton(View view){
		Intent intent = new Intent(this,LostPasswordActivity.class);
		startActivity(intent);
	}
	
	public void onClickLoginButton(View view){
		asyncLogin();
		//doLogin();
				
	}
	
	private void asyncLogin(){
		
		//ServerAsyncTask loginTask = new ServerAsyncTask();
		
		inputEmail = (EditText) findViewById(R.id.email);
		inputPassword = (EditText) findViewById(R.id.password);
		loginErrorMsg = (TextView) findViewById(R.id.login_error);
		
		String email = inputEmail.getText().toString();
		String password = inputPassword.getText().toString();
		if(InputCheck.isStringInputValid(this, email, password)){
			ServerRequestObject loginData = new ServerRequestObject();
			loginData.setUrl("http://172.9.69.232/wheelshare/postlogin.php");
			loginData.putParameter("username", email);
			loginData.putParameter("password", password);
			
			ServerAsyncTask login = new ServerAsyncTask();
			try {
				String str = login.execute(loginData).get();
				if(str ==null || str ==""){
						Toast.makeText(this, "NO NETWORK CONNECTION", Toast.LENGTH_LONG).show();
				}
				else{
					//Toast.makeText(this, "Result: " + str, Toast.LENGTH_SHORT).show();
					if(str.contains("Authorized")) {	
						// user successfully logged in 
						// start up homepage/dashboard activity
						//get UID to pass on.  UID is concatenated to Authorized
						String str2 = str.replaceAll("\\D+","");
						
						//push the UID to SharedPreferences for persistence
						SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
					    SharedPreferences.Editor editor = settings.edit();
					    editor.putString("UID", str2);
					    editor.putString("USERNAME", email);
					    // Commit the edits!
					    editor.commit();
						
						if(preset == null){
							Intent i = new Intent(this, LoadGCMRegisterActivity.class);
						
							startActivity(i);
						
							finish(); 	// close Login screen
						}else{
							Intent i = new Intent(this, PasswordResetActivity.class);
							i.putExtra("UID", str2);
							startActivity(i);
							finish();
						}
					} else {
						//Toast.makeText(this,  "Login Result: " + str, Toast.LENGTH_LONG).show();
				        //Error in login, server returned something other than Authorized
						loginErrorMsg.setText("Invalid Username/Password");
						
				
					}
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//new ServerAsyncTask().execute(loginData);
	}
	

	
public static String convertStreamToString(java.io.InputStream is) {
	    //utility function for parsing the HTTP response 
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
}


/*
public class EndpointsTask extends AsyncTask<UserData, Void, Void> {
    @Override
	protected Void doInBackground(UserData... userdata) {

      Userdataendpoint.Builder endpointBuilder = new Userdataendpoint.Builder(
          AndroidHttp.newCompatibleTransport(),
          new JacksonFactory(),
          new HttpRequestInitializer() {
          public void initialize(HttpRequest httpRequest) { }
          });
  endpointBuilder.setApplicationName("electric-sheep160");
  Userdataendpoint endpoint = CloudEndpointUtils.updateBuilder(
  endpointBuilder).build();
  try {
      //UserData user = new UserData().setMessage("test");
      
      //String userID = new Date().toString();
      //user.setId(userID);

      //UserData result = 
	  //endpoint.insertSQLUserData(userdata[0]).execute();
      //endpoint.insertUserData(userdata[0]).execute();
      endpoint.getUserData(userdata[0]).execute();
      } catch (IOException e) {
    e.printStackTrace();
  }
      return null;
    }
}
*/
	
	/*  //code which didn't work, left here for educational purposes
	protected void tryLogin(String mUsername, String mPassword)
    {           
        HttpURLConnection connection;
       OutputStreamWriter request = null;

            URL url = null;   
            String response = null;         
            String parameters = "username="+mUsername+"&password="+mPassword;   

            try
            {
                //url = new URL("http://electric-sheep160.appspot.com/login");
                url = new URL("http://localhost:8888/login");
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestMethod("POST");    

                request = new OutputStreamWriter(connection.getOutputStream());
                request.write(parameters);
                request.flush();
                request.close();            
                String line = "";               
                InputStreamReader isr = new InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(isr);
                StringBuilder sb = new StringBuilder();
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                // Response from server after login process will be stored in response variable.                
                response = sb.toString();
                // You can perform UI operations here
                Toast.makeText(this,"Message from Server: \n"+ response, 0).show();             
                isr.close();
                reader.close();

            }
            catch(IOException e)
            {
            	Toast.makeText(this,  "Login Error" + response, 0).show();
                // Error
            }
    }
	*/

    


	/** Called when the user clicks the register button. */
	
}
