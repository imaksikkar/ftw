package com.indigohate;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;


public class AllMyRidesActivity extends Fragment implements OnTabChangeListener{

	private static final String TAG = "AllMyRidesActivity";
	public static final String TAB_CONFIRMED = "confirmed";
	public static final String TAB_PENDING ="pending";
	
	private View mRoot;
	private TabHost mTabHost;
	private int mCurrentTab;

	
	//Possibly attaches activity to the tab...?
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
		
	//Should be called when created
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG,"createdview");
		mRoot = inflater.inflate(R.layout.activity_all_my_rides, null);
		mTabHost = (TabHost) mRoot.findViewById(android.R.id.tabhost);
		setupTabs(); // will handle the setting up of tabs 1 and 2
		return mRoot; // returns the current view....?
	}
	//sets up the entire activity
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//possible to save state..?
		setRetainInstance(true);

		mTabHost.setOnTabChangedListener(this); // obviously so the tabhost can respond to tab changes
		mTabHost.setCurrentTab(mCurrentTab);
		// manually start loading stuff in the first tab
		updateTab(TAB_CONFIRMED, R.id.tab_1);
	}

	private void setupTabs() {
		Log.d(TAG,"setting up tabs");
		mTabHost.setup(); // important!
		mTabHost.addTab(newTab(TAB_CONFIRMED, R.string.tab_confirmed, R.id.tab_1));
		mTabHost.addTab(newTab(TAB_PENDING, R.string.tab_pending, R.id.tab_2));
	}

	private TabSpec newTab(String tag, int labelId, int tabContentId) {
		Log.d(TAG, "buildTab(): tag=" + tag);

		View indicator = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab,
				(ViewGroup) mRoot.findViewById(android.R.id.tabs), false);
		((TextView) indicator.findViewById(R.id.text)).setText(labelId);

		TabSpec tabSpec = mTabHost.newTabSpec(tag);
		if(tag.equals(TAB_CONFIRMED)){
			tabSpec.setIndicator(tag,getResources().getDrawable(R.drawable.ic_confirmed_tab));
		}
		else{
			tabSpec.setIndicator(tag,getResources().getDrawable(R.drawable.ic_pending_tab));
		}
		//tabSpec.setIndicator(indicator);
		tabSpec.setContent(tabContentId);
		return tabSpec;
	}
	
	@Override
	public void onTabChanged(String tabId) {
		Log.d(TAG, "onTabChanged(): tabId=" + tabId);
		if (TAB_CONFIRMED.equals(tabId)) {
			updateTab(tabId, R.id.tab_1);
			mCurrentTab = 0;
			return;
		}
		if (TAB_PENDING.equals(tabId)) {
			updateTab(tabId, R.id.tab_2);
			mCurrentTab = 1;
			return;
		}
	}
	
	private void updateTab(String tabId, int placeholder) {
		Log.d(TAG,"inside update");
		FragmentManager fm = getFragmentManager();
		if (fm.findFragmentByTag(tabId) == null) {
			Log.d(TAG,"update not null");
			if(tabId == TAB_CONFIRMED){
				Log.d(TAG,"update confirm");
				fm.beginTransaction()
						.replace(placeholder, new ConfirmedTab(tabId), tabId)
						.commit();
			}
			else if (tabId == TAB_PENDING){
				Log.d(TAG,"update pending");
				fm.beginTransaction()
				.replace(placeholder, new PendingTab(tabId), tabId)
				.commit();
			}
		}
	}



}
