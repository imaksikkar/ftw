package com.indigohate;

import java.util.concurrent.ExecutionException;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ExistingOfferOrRequestActivity extends Activity {

	public String rideDataItems[];
	public String uid;
	public String rideId;
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);		
		uid = settings.getString("UID", "Invalid");
		
		rideId = getIntent().getStringExtra("ride");
		String rideType = getIntent().getStringExtra("rideType");
		
		if(rideType.contains("Offer")){
			String owner = getIntent().getStringExtra("owner");
			String date = getIntent().getStringExtra("date");
			String time = getIntent().getStringExtra("time");
			String startLocation = getIntent().getStringExtra("startLocation");
			String destination = getIntent().getStringExtra("destination");
			String price = getIntent().getStringExtra("price");
			String seatsLeft = getIntent().getStringExtra("seatsLeft");
			
			setContentView(R.layout.activity_your_offer_details);
			TextView txtOwner = (TextView) findViewById(R.id.existing_offer_owner);
			TextView txtDate = (TextView) findViewById(R.id.existing_offer_date);
			TextView txtFrom = (TextView) findViewById(R.id.existing_offer_from);
			TextView txtTo = (TextView) findViewById(R.id.existing_offer_to);
			TextView txtCost = (TextView) findViewById(R.id.existing_offer_cost);
			TextView txtSeats = (TextView) findViewById(R.id.existing_offer_seats_left);
			txtOwner.setText(owner);
			txtDate.setText(date);
			txtFrom.setText(startLocation);
			txtTo.setText(destination);
			txtCost.setText(price);
			txtSeats.setText(seatsLeft);
		}
		else{
			String date = getIntent().getStringExtra("date");
			String time = getIntent().getStringExtra("time");
			String startLocation = getIntent().getStringExtra("startLocation");
			String destination = getIntent().getStringExtra("destination");
			setContentView(R.layout.activity_your_request_details);
			TextView txtDate = (TextView) findViewById(R.id.existing_request_date);
			TextView txtFrom = (TextView) findViewById(R.id.existing_request_from);
			TextView txtTo = (TextView) findViewById(R.id.existing_request_to);
			txtDate.setText(date);
			txtFrom.setText(startLocation);
			txtTo.setText(destination);
		}
		/*
		String detailItems[] = null;
		String rideDetails = null;
		String rideData = getIntent().getStringExtra("ride");
		rideDataItems = rideData.split(" ");
		rideId = rideDataItems[0];
		detailItems = getRideDetails(rideDataItems[0]);
		
		for(int i = 0; i < rideDataItems.length; i++)
			Log.d("existing details", "rideDataItems[i] = "+String.valueOf(rideDataItems[i]));
		
		if(rideDataItems[1].equals("Offered")){
			setContentView(R.layout.activity_your_offer_details);
			TextView owner = (TextView) findViewById(R.id.existing_offer_owner);
			TextView date = (TextView) findViewById(R.id.existing_offer_date);
			TextView from = (TextView) findViewById(R.id.existing_offer_from);
			TextView to = (TextView) findViewById(R.id.existing_offer_to);
			TextView cost = (TextView) findViewById(R.id.existing_offer_cost);
			TextView seats = (TextView) findViewById(R.id.existing_offer_seats_left);
			
			Toast.makeText(getBaseContext(), rideDataItems[0], Toast.LENGTH_LONG).show();
			
			if(detailItems.length > 11){
			owner.setText(detailItems[1]);
			date.setText(detailItems[6]);
			from.setText(detailItems[4]);
			to.setText(detailItems[5]);
			cost.setText("$"+detailItems[11]);
			seats.setText(detailItems[2]);
			}
			
		}else if(rideDataItems[1].equals("Requested")){
			setContentView(R.layout.activity_your_request_details);
			TextView date = (TextView) findViewById(R.id.existing_request_date);
			TextView from = (TextView) findViewById(R.id.existing_request_from);
			TextView to = (TextView) findViewById(R.id.existing_request_to);
			
			Toast.makeText(getBaseContext(), rideDataItems[0], Toast.LENGTH_LONG).show();
			
			if(rideDataItems.length > 8){
				date.setText(rideDataItems[4]);
				from.setText(rideDataItems[6]);
				to.setText(rideDataItems[8]);
			}
			
		}
		Log.d("existing details", "type is "+String.valueOf(rideDataItems[1]));
		*/
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ride_detail, menu);
		return true;
	}

	private String[] getRideDetails(String rideId){
		String url = "http://172.9.69.232/wheelshare/getridebyidpost.php";
		
		ServerRequestObject details = new ServerRequestObject();
		ServerAsyncTask getDetails = new ServerAsyncTask();
		
		details.setUrl(url);
		details.putParameter("rideid", rideId);
		
		String resultLines[] = null;
		String result = null;
		try {
			result = getDetails.execute(details).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(result != null){
			resultLines = result.split(",");
		}
		
		return resultLines;
		
	}
	private String[] getRequestDetails(String rideId){
		String url = "http://172.9.69.232/wheelshare/getridebyidpost.php";
		
		ServerRequestObject details = new ServerRequestObject();
		ServerAsyncTask getDetails = new ServerAsyncTask();
		
		details.setUrl(url);
		details.putParameter("rideid", rideId);
		
		String resultLines[] = null;
		String result = null;
		try {
			result = getDetails.execute(details).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(result != null){
			resultLines = result.split(",");
		}
		
		return resultLines;
		
	}
	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
}
