package com.indigohate;

import java.util.concurrent.ExecutionException;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class RequestDetailsActivity extends Activity {
	
	//public String rideDataItems[];
	public String uid;
	public String rideId;
	public String offerId;
	public String requesterId;
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_request_details);
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);		
		uid = settings.getString("UID", "Invalid");
		
		
		String detailItems[] = null;
		String rideDetails = null;
		rideId = getIntent().getStringExtra("ride");
		offerId = getIntent().getStringExtra("rideId");
		detailItems = getRideDetails(rideId);
		
		TextView requesterUsername = (TextView) findViewById(R.id.details_requester);
		TextView date = (TextView) findViewById(R.id.details_date);
		TextView from = (TextView) findViewById(R.id.details_from);
		TextView to = (TextView) findViewById(R.id.details_to);
		
		Toast.makeText(getBaseContext(), detailItems[0], Toast.LENGTH_LONG).show();
		
		requesterId = detailItems[1];
		for(int i=0; i<detailItems.length; i++)
			Log.d("request details", "detailItems["+i+"] = "+detailItems[i]);
		
		//if(detailItems.length > 11){
		requesterUsername.setText(detailItems[2]);
		date.setText(detailItems[5]);
		from.setText(detailItems[3]);
		to.setText(detailItems[4]);
		// }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.request_details, menu);
		return true;
	}

	private String[] getRideDetails(String rideId){
		String url = "http://172.9.69.232/wheelshare/getrequestbyidpost.php";
		
		ServerRequestObject details = new ServerRequestObject();
		ServerAsyncTask getDetails = new ServerAsyncTask();
		
		details.setUrl(url);
		details.putParameter("rideid", rideId);
		
		String resultLines[] = null;
		//String[] detailItems = getRideDetails(rideId);
		String result = null;
		try {
			result = getDetails.execute(details).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(result != null){
			resultLines = result.split(",");
		}
		
		return resultLines;
		
	}
	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	private String addPendingPassenger(){
		String result = null;
		ServerRequestObject join = new ServerRequestObject();
		ServerAsyncTask doJoin = new ServerAsyncTask();
		
		join.setUrl("http://172.9.69.232/wheelshare/addpassenger.php");
		join.putParameter("rideid", offerId);
		join.putParameter("requesterid", requesterId);
		join.putParameter("uid", uid);
		Log.d("request details", "offerId: "+offerId);
		Log.d("request details", "requesterId: "+requesterId);
		Log.d("request details", "uid: "+uid);
		
		try {
			result = doJoin.execute(join).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		return result;
	}
	
	public void onAddPassengerButtonClick(View view){
		String result = addPendingPassenger();
		if(result == "0"){
			Toast.makeText(this,  "Error joining ride: " + result, Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(this,  "Joined Ride #" + rideId, Toast.LENGTH_LONG).show();
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
		}
	}

}
