package com.indigohate;

import java.util.concurrent.ExecutionException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


@SuppressLint("ValidFragment")
public class PaymentInfoTab extends Fragment{
	
	private static final String TAG = "PaymentTab";
	private String mTag;
	private LayoutInflater mInflater;
	
	public String previousActivity = null;
    public String userdataResult;
    public String uid;
    public static final String EXTRA_UID = "com.indigohate.extra.UID";
    public static final String PREFS_NAME = "WheelsharePrefsFile";
    public String cardType;
    public String update = "set";
    public String firstname;
    public String lastname;
    public String cardnumber;
    public String expMonth;
    public String expYear;
    public String streetaddress;
    public String city;
    public String state;
    public String country;
    public String zip;
	
	
	public PaymentInfoTab(){
		
	}
	
	public PaymentInfoTab(String tag){
		this.mTag = tag;
		Log.d(TAG, "Constructor: tag=" + tag);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View V = inflater.inflate(R.layout.payment_tab_fragment, container, false);
        Log.d(TAG, "created");
          return V;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		setRetainInstance(true);
		mInflater = LayoutInflater.from(getActivity());
		asyncGetPaymentInfo();
	}
	
	
    private void asyncGetPaymentInfo()
    {
    	SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
    	uid = settings.getString("UID", "Invalid"); 
    	ServerRequestObject paymentData = new ServerRequestObject();
    	paymentData.setUrl("http://172.9.69.232/wheelshare/paymentinfo.php");
    	paymentData.putParameter("uid", uid);
    	ServerAsyncTask paymentInfo = new ServerAsyncTask();
    	try{
    		String str = paymentInfo.execute(paymentData).get();
    		//if(str == null){
            //if(str.length() < 1){
    			//update = "set";
            	//Payment info not yet set, so database is instructed to create new record
            	
            //}else{
    		if(str ==null || str==""){
    			Toast.makeText(getActivity(), "Unavailable Network", Toast.LENGTH_LONG).show();
    		}
    		else if(str.length() > 0){
            	update = "update";
            	//record exists, update, don't create new record
            
            	String[] paymentdataLines = str.split(",");
                //setContentView(R.layout.activity_payment_information);
                
                EditText inputFirstName = (EditText) getView().findViewById(R.id.text_card_first_name);
                EditText inputLastName = (EditText) getView().findViewById(R.id.text_card_last_name);
                EditText inputCardNumber = (EditText) getView().findViewById(R.id.text_card_number);
                EditText inputExpMonth = (EditText) getView().findViewById(R.id.exp_date_month);
                EditText inputExpYear = (EditText) getView().findViewById(R.id.exp_date_year);
                EditText inputStreetAddress = (EditText) getView().findViewById(R.id.text_address);
                EditText inputCity = (EditText) getView().findViewById(R.id.text_city);
                EditText inputState = (EditText) getView().findViewById(R.id.text_state);
                RadioButton creditType = (RadioButton)getView().findViewById(R.id.radio_credit);
                RadioButton debitType = (RadioButton)getView().findViewById(R.id.radio_debit);
    			
                //EditText inputCountry = (EditText) findViewById(R.id.text_country);
                EditText inputZip = (EditText) getView().findViewById(R.id.text_zip_code);
                
                if(paymentdataLines.length > 9){
	                inputFirstName.setText(paymentdataLines[0]);
	                inputLastName.setText(paymentdataLines[1]);
	                inputCardNumber.setText(paymentdataLines[2]);
	                inputExpMonth.setText(paymentdataLines[4]);
	                inputExpYear.setText(paymentdataLines[3]);
	                inputStreetAddress.setText(paymentdataLines[5]);
	                inputCity.setText(paymentdataLines[6]);
	                inputState.setText(paymentdataLines[7]);
	                inputZip.setText(paymentdataLines[8]);
	                cardType = paymentdataLines[9];
	                if(cardType.contains("debit")){
	                	debitType.setChecked(true);
	                }
	                else if (cardType.contains("credit")){
	                	creditType.setChecked(true);
	                }
	                else{
	                	Log.d(TAG," type is actually "+ cardType);
	                }
	                
                }else{
                	update="set";
                }
                //else{
                	//Log.d("paymentinfo", "not enough payment data: " + String.valueOf(paymentdataLines.length));
                //}
            }
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void asyncUpdatePaymentInfo()
    {
    	ServerRequestObject paymentInfo = new ServerRequestObject();
    	paymentInfo.setUrl("http://172.9.69.232/wheelshare/updatepaymentinfo.php");
    	EditText inputFirstName = (EditText) getView().findViewById(R.id.text_card_first_name);
        EditText inputLastName = (EditText) getView().findViewById(R.id.text_card_last_name);
        EditText inputCardNumber = (EditText) getView().findViewById(R.id.text_card_number);
        EditText inputExpMonth = (EditText) getView().findViewById(R.id.exp_date_month);
        EditText inputExpYear = (EditText) getView().findViewById(R.id.exp_date_year);
        EditText inputStreetAddress = (EditText) getView().findViewById(R.id.text_address);
        EditText inputCity = (EditText) getView().findViewById(R.id.text_city);
        EditText inputState = (EditText) getView().findViewById(R.id.text_state);
        EditText inputCountry = (EditText) getView().findViewById(R.id.text_country);
        EditText inputZip = (EditText) getView().findViewById(R.id.text_zip_code); 
        RadioButton creditType = (RadioButton)getView().findViewById(R.id.radio_credit);
        RadioButton debitType = (RadioButton)getView().findViewById(R.id.radio_debit);
		
        TextView errorMsg = (TextView) getView().findViewById(R.id.payment_info_error_msg);
		
		firstname = inputFirstName.getText().toString();
	    lastname = inputLastName.getText().toString();
	    cardnumber = inputCardNumber.getText().toString();
	    expMonth = inputExpMonth.getText().toString();
	    expYear = inputExpYear.getText().toString();
	    streetaddress = inputStreetAddress.getText().toString();
	    city = inputCity.getText().toString();
	    state = inputState.getText().toString();
	    country = inputCountry.getText().toString();
	    zip = inputZip.getText().toString();
         if(creditType.isChecked())
        	 cardType = "credit";
         else if (debitType.isChecked())
        	 cardType="debit";
         else
        	 Log.d(TAG,"invalid card type");
         if (!verifyCardNumber(cardnumber)) {
        	 errorMsg.setText("invalid card number");
        	 Log.d("registerpayment", "invalid card number");
         }
         else {		// valid card number
        	 Log.d("registerpayment", "valid card number");
        	 paymentInfo.putParameter("uid", uid);
             paymentInfo.putParameter("firstname", firstname);
             paymentInfo.putParameter("lastname", lastname);
             paymentInfo.putParameter("cardnumber", cardnumber);
             paymentInfo.putParameter("expirymonth", expMonth);
             paymentInfo.putParameter("expiryyear", expYear);
             paymentInfo.putParameter("type", cardType);
             paymentInfo.putParameter("address", streetaddress);
             paymentInfo.putParameter("city", city);
             paymentInfo.putParameter("state", state);
             paymentInfo.putParameter("zip", zip);
             paymentInfo.putParameter("update", update);
             paymentInfo.putParameter("type", cardType);
             ServerAsyncTask updatePayment = new ServerAsyncTask();
             try{
            	 String str = updatePayment.execute(paymentInfo).get();
            	 Toast.makeText(getActivity().getApplicationContext(),  "Result: " + str, Toast.LENGTH_LONG).show();
             }catch (InterruptedException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		} catch (ExecutionException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		}
             
            // switch to login or profile screen depending on what activity
            // this page was opened from (ProfileActivity or UserRegister)
            /*if(previousActivity.equals("profile")) {
	 			Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
	 			startActivity(intent);
				finish();
            }
            if(previousActivity.equals("register")) {
            	Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            	startActivity(intent);
    			finish();
            } */
            
        }
         
         
    }
    public String formatDateString(int year, int month, int day) {
        String monthStr = "" + month;
        String dayStr = "" + day;
        if(month < 10)
                monthStr = "0" + month;
        if(day < 10)
                dayStr = "0" + day;
        return(""+year+"-"+monthStr+"-"+dayStr);
}
	
    // Luhn algorithm to verify that card number is valid
    private boolean verifyCardNumber(String cardNumber) {
    	if(!(cardNumber.length() == 15 || cardNumber.length() == 16))
    		return (false);
        int sum = 0;
        boolean alternate = false;
        for (int i = cardNumber.length() - 1; i >= 0; i--)
        {
            int n = Integer.parseInt(cardNumber.substring(i, i + 1));
            if (alternate) 
            {
                n *= 2;
                if (n > 9)
                {
                   n = (n % 10) + 1;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        return (sum % 10 == 0);  
    }   
}
