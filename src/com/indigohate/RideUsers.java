package com.indigohate;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import java.util.concurrent.ExecutionException;

public class RideUsers extends ListActivity {
	
	public String rid;
	ArrayList<String> uidLinesList = new ArrayList<String>();
	public String uid;
	public String ridedataResult;
	public static final String EXTRA_UID = "com.indigohate.extra.RID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
    private ListView searchResultsList;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ride_users);
		rid= getIntent().getStringExtra("rid");
		Toast.makeText(getBaseContext(), rid, Toast.LENGTH_LONG).show();
		asyncGetRideUsers();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ride_users, menu);
		return true;
	}
	
	

	//POPULATE LIST CODE HERE
	 private void asyncGetRideUsers()
	    {
	    	ArrayList<String> dataLinesList = new ArrayList<String>();
	    	ServerRequestObject rideUsersData = new ServerRequestObject();
	    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	    	rid = settings.getString("RID", "Invalid");
	    	rideUsersData.setUrl("http://172.9.69.232/wheelshare/listpassengers.php"); 
	    	rideUsersData.putParameter("rideid", rid);
	    	ServerAsyncTask getRideUsers = new ServerAsyncTask();
	    	try{ 																				
	    		String str = /*"1,Jim;2,Bob;3,Tom;4,Hank;";*/getRideUsers.execute(rideUsersData).get();    
	    		String[] ridedataLines = str.split(";");
	    		try{
	    			 for(int i=0; i < ridedataLines.length; i++){
	    			    uidLinesList.add(ridedataLines[i].substring(0,1));
	    				dataLinesList.add(ridedataLines[i].substring(2,ridedataLines[i].length()));
	    				
	    			 }
	    			
	    			}catch(NullPointerException e){
	    				e.printStackTrace();
	    			}
	    		
	    	}catch (InterruptedException e) {												
				 //TODO Auto-generated catch block												
				e.printStackTrace();															
			} catch (ExecutionException e) {												
				 //TODO Auto-generated catch block												
				e.printStackTrace();															
			}																					
	    	searchResultsList = getListView();	
			
			SearchResultsListAdapter adapter = new SearchResultsListAdapter(this,
					//R.layout.listview_item_row, 
					android.R.layout.simple_list_item_1,
					dataLinesList);
			
			searchResultsList.setAdapter(adapter);
	    }
	
	
	//CLICKING CODE HERE
	@Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        String riderId = uidLinesList.get(position);
        Intent intent = new Intent(this,MakeCommentActivity.class);
        Bundle extras= new Bundle();
        extras.putString("UID", riderId);
        intent.putExtras(extras);
		startActivity(intent);
		finish();
      //Toast.makeText(getBaseContext(), riderId, Toast.LENGTH_LONG).show();
        
    }
}
    
	
	
	
	

	
	
	