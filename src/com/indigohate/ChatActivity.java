package com.indigohate;

import java.util.concurrent.ExecutionException;

import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ChatActivity extends Activity {
	public String uid;
	public String UserName;
	public String rideid;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		uid = settings.getString("UID", "Invalid");
		//UserName = settings.getString("USERNAME", "Invalid");
		rideid = getIntent().getStringExtra("rideid");
		
		getMessages();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chat, menu);
		return true;
	}
	
	public void onSendClicked(View view) {
		EditText newMessage = (EditText) findViewById(R.id.chat_message_text);
		String message = newMessage.getText().toString();
		
		ServerRequestObject outgoing = new ServerRequestObject();
		ServerAsyncTask sendMessage = new ServerAsyncTask();
		
		outgoing.setUrl("http://172.9.69.232/wheelshare/postusermessage.php");
		outgoing.putParameter("uid", uid);
		outgoing.putParameter("rideid", rideid);
		outgoing.putParameter("message", message);
		
		String result = null;
		
		try {
			result = sendMessage.execute(outgoing).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(result.contains("1")){
			getMessages();
			newMessage.setText("");
		}
		
	}

	private void getMessages(){
		ServerRequestObject messageData = new ServerRequestObject();
		ServerAsyncTask getMessages = new ServerAsyncTask();
		
		messageData.setUrl("http://172.9.69.232/wheelshare/getusermessages.php");
		messageData.putParameter("rideid", rideid);
				
		String allMessages = null;
		try {
			allMessages = getMessages.execute(messageData).get();
			TextView chatContents = (TextView) findViewById(R.id.chat_contents);
			chatContents.setText(allMessages);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
