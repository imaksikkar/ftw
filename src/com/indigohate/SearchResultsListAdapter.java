package com.indigohate;



import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import android.widget.ArrayAdapter;


public class SearchResultsListAdapter extends ArrayAdapter<String> {
	Context context;
	int layoutResourceId;
	//List<RideContent.RideItem> data = null;
	//RideContent data[];
	HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
	ArrayList<String> data;
	
	public SearchResultsListAdapter(Context context, int layoutResourceId, ArrayList<String> data)
	{
		super( context, layoutResourceId, data);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.data = data;
		
		for (int i = 0; i < data.size(); ++i) {
	        mIdMap.put(data.get(i), i);
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	      String item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }
		
}	
		
	
	/*
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		RideHolder holder = null;
		
		if(row == null)
		{
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			
			holder = new RideHolder();
			//holder.txtFirstName = (TextView)row.findViewById(R.id.txtFirstName);
			//holder.txtLastName = (TextView)row.findViewById(R.id.txtLastName);
			//holder.txtPassengers = (TextView)row.findViewById(R.id.txtPassengers);
			holder.txtStartLocation = (TextView)row.findViewById(R.id.txtStartLocation);
			holder.txtEndLocation = (TextView)row.findViewById(R.id.txtEndLocation);
			holder.txtDateTime = (TextView)row.findViewById(R.id.txtDate);
			row.setTag(holder);
		}
		else
		{
			holder = (RideHolder)row.getTag();
		}
		
		RideContent rideTransaction = data[position];
		//holder.txtFirstName.setText(rideTransaction.firstName);
		//holder.txtLastName.setText(rideTransaction.lastName);
		//holder.txtPassengers.setText(rideTransaction.passengers.get(0));
		holder.txtStartLocation.setText(rideTransaction.getFromLocation());
		holder.txtEndLocation.setText(rideTransaction.getToDestination());
		holder.txtDateTime.setText(rideTransaction.getRideDate());
		
		return row;
	}
	
	static class RideHolder
	{
		//TextView txtFirstName;
		//TextView txtLastName;
		//TextView txtPassengers;
		TextView txtStartLocation;
		TextView txtEndLocation;
		TextView txtDateTime;
	}
}
*/
