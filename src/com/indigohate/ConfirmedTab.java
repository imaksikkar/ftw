package com.indigohate;

import java.util.ArrayList;

import com.indigohate.classes.JoinedRide;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


@SuppressLint("ValidFragment")
public class ConfirmedTab extends Fragment{
	private static final String TAG = "ConfirmedList";

	private String mTag;
	//private MyAdapter mAdapter;
	private ArrayList<String> mItems;
	private LayoutInflater mInflater;
	private int mTotal;
	private int mPosition;
	JoinedRide[] data;
	public ConfirmedTab(){
		
	}
	
	public ConfirmedTab(String tag){
		this.mTag= tag;
		Log.d(TAG, "Constructor: tag=" + tag);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View V = inflater.inflate(R.layout.confirmed_fragment, container, false);
        Log.d(TAG, "created");
          return V;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d(TAG,"wtf");
		// this is really important in order to save the state across screen
		// configuration changes for example
		setRetainInstance(true);

		mInflater = LayoutInflater.from(getActivity());
		
		FragmentManager fm = getFragmentManager();
		if(getActivity() != null){
			Log.d(TAG,"get activity is not null! yay");
			AllMyRidesFragmentActivity tabHost = (AllMyRidesFragmentActivity)getActivity();
			data = tabHost.getJoinedRides();
			mTotal = data.length;
			ListView listView = (ListView)getView().findViewById(R.id.confirmedListView);
			Log.d(TAG,"right before adapter");
			ConfirmedArrayAdapter cAdapter = new ConfirmedArrayAdapter(getActivity(),
					R.layout.confirmed_listview_item,
					data);
			Log.d(TAG,"just made the adapter");

			listView.setOnItemClickListener(new OnItemClickListener()
				{
					@Override 
					public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
				    { 
						//Navigate to the ride details here
						JoinedRide joinedRide = data[position];
						String rideId = joinedRide.rideId;
						String owner = joinedRide.driver;
						String date = joinedRide.date;
						String time = joinedRide.time;
						String startLocation = joinedRide.startLocation;
						String destination = joinedRide.destination;
						String price = joinedRide.price;
						String seatsLeft = joinedRide.seatsLeft;
						Intent intent = new Intent(getActivity(),JoinedRideDetail.class);
						Bundle extras = new Bundle();
						extras.putString("rideId",rideId);
						extras.putString("owner", owner);
						extras.putString("date", date);
						extras.putString("time", time);
						extras.putString("startLocation", startLocation);
						extras.putString("destination", destination);
						extras.putString("price", price);
						extras.putString("seatsLeft", seatsLeft);
						intent.putExtras(extras);
						getActivity().startActivity(intent);
						//String dataa = ((TextView)arg1.findViewById(R.id.txtDriverConfirmed)).getText().toString();
				        //Toast.makeText(getActivity().getApplicationContext(), "" + position + " " + rideId, Toast.LENGTH_LONG).show();
				    }
				}
			);
			listView.setAdapter(cAdapter);
			Log.d(TAG,"just set the adapter");
		}
		else{
			Log.d(TAG,"shit..");
		}
	}

	public class ConfirmedArrayAdapter extends ArrayAdapter<JoinedRide>{
		Context context;
		int layoutResourceId;
		JoinedRide data[] = null;
		private String TAG = "confirmedadapter";
		
		public ConfirmedArrayAdapter(Context context, int layoutResourceId, JoinedRide[] data){
			super(context,layoutResourceId,data);
			this.context = context;
			this.layoutResourceId = layoutResourceId;
			this.data = data;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View row = convertView;
			ConfirmedRideHolder holder = null;
			Log.d(TAG,"in getview");
			if(row == null)
			{
				Log.d(TAG,"in in");
				LayoutInflater inflater = ((Activity)context).getLayoutInflater();
				Log.d(TAG,"after activity cast");
				row = inflater.inflate(layoutResourceId, parent, false);
				Log.d(TAG,"inflating row");
				holder = new ConfirmedRideHolder();
				Log.d(TAG,"before textview cast");
				holder.textStatus = (TextView)row.findViewById(R.id.txtConfirmedStatus);
				holder.textStartLocation = (TextView)row.findViewById(R.id.txtStartLocConfirmed);
				holder.textDestination = (TextView)row.findViewById(R.id.txtDestinationConfirmed);
				holder.textDate = (TextView)row.findViewById(R.id.txtConfirmedDate);
				holder.textTime = (TextView)row.findViewById(R.id.txtConfirmedTime);
				holder.textPrice = (TextView)row.findViewById(R.id.txtPriceConfirmed);
				holder.textSeatsLeft = (TextView)row.findViewById(R.id.txtSeatsLeftConfirmed);
				holder.textDriver = (TextView)row.findViewById(R.id.txtDriverConfirmed);
				Log.d(TAG,"before settag");
				row.setTag(holder);
			}
			else
			{
				holder = (ConfirmedRideHolder)row.getTag();
			}
			
			JoinedRide result = data[position];
			holder.textStatus.setText(result.status);
			holder.textStartLocation.setText(result.startLocation);
			holder.textDestination.setText(result.destination);
			holder.textDate.setText(result.date);
			holder.textTime.setText(result.time);
			holder.textPrice.setText(result.price);
			holder.textSeatsLeft.setText(result.seatsLeft);
			holder.textDriver.setText(result.driver);
			
			
			return row;
		}
		
		class ConfirmedRideHolder{
			TextView textStatus;
			TextView textStartLocation;
			TextView textDestination;
			TextView textDate;
			TextView textTime;
			TextView textSeatsLeft;
			TextView textPrice;
			TextView textDriver;
		}
	}
	
}
