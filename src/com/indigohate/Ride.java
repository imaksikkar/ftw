package com.indigohate;

//import com.google.appengine.api.datastore.Entity;






public class Ride {
	
	private String id;
	
	private String owner;
	//private String passenger[];
	//private List<String> passengerList = new ArrayList<String>();
	private String numberSeats;
	private String full;
	private String fromLocation;
	private String toDestination;
	private String rideDate;
	private String recurring;
	private String fromDate;
	private String toDate;
	private String frequency;
	private String costPerMile;
	private String startTime;
	
	public void setStartTime(String time){
		this.startTime=time;
	}
	public String getStartTime(){
		return startTime;
	}
	
	public void setOwner(String owner){
		this.owner=owner;
	}
	
	public String getOwner(){
		return owner;
	}
	
	
	public void setNumberSeats(String seats){
		this.numberSeats=seats;
	}
	
	public String getNumberSeats(){
		return numberSeats;
	}
	
	public void setFull(String full){
		this.full=full;
	}
	
	public String getFull(){
		return full;
	}
	
	public void setFromLocation(String start){
		this.fromLocation=start;
	}
	
	public String getFromLocation(){
		return fromLocation;
	}
	
	public void setToDestination(String to){
		this.toDestination=to;
	}
	
	public String getToDestination(){
		return toDestination;
	}
	
	public void setRideDate(String ridedate){
		this.rideDate=ridedate;
	}
	
	public String getRideDate(){
		return rideDate;
	}
	
	public void setRecurring(String recur){
		this.recurring=recur;
	}
	
	public String getRecurring(){
		return recurring;
	}
	
	public void setFromDate(String from){
		this.fromDate=from;
	}
	
	public String getFromDate(){
		return fromDate;
	}
	
	public void setToDate(String to){
		this.toDate=to;
	}
	
	public String getToDate(){
		return toDate;
	}
	
	public void setFrequency(String freq){
		this.frequency=freq;
	}
	
	public String getFrequency(){
		return frequency;
	}
	
	public void setCostPerMile(String cents){
		this.costPerMile=cents;
	}
	
	public String getCostPerMile(){
		return costPerMile;
	}

	public String getId() {
		return id;
	}
	public void setId(String id){
		this.id=id;
	}
}