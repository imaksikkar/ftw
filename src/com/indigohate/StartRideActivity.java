package com.indigohate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.indigohate.classes.RideOffer;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

public class StartRideActivity extends Activity {
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	public String uid;
	private LocationManager mgr;
	public String ownerId;
	public String owner;
	public String rideId;
	public String rideDate;
	public String rideCost;
	public String fromLocation;
	public String toLocation;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_ride);
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		uid = settings.getString("UID", "Invalid");
		
		Bundle extras = getIntent().getExtras(); 
		if(extras !=null) {
		    rideId = extras.getString("RIDE_ID");
		    String[] detailItems = getRideDetails(rideId);
		    fromLocation = detailItems[4];
		    toLocation = detailItems[5];
		    
		    TextView ownerText = (TextView) findViewById(R.id.start_ride_driver);
		    TextView costText = (TextView) findViewById(R.id.start_ride_offer_cost);
		    TextView destText = (TextView) findViewById(R.id.start_ride_destination);
		    if(detailItems.length > 11){
				ownerText.setText(detailItems[1]);
				destText.setText(detailItems[5]);
				costText.setText("$"+detailItems[11]);
			}

		    
		}
		
		
		mgr=(LocationManager)getSystemService(LOCATION_SERVICE);
	    
	    mgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                3600, 2,
                onLocationChange);
	    showCurrentLocation();
		
	}

	private String[] getRideDetails(String rideId) {
		String url = "http://172.9.69.232/wheelshare/getridebyidpost.php";
		
		ServerRequestObject details = new ServerRequestObject();
		ServerAsyncTask getDetails = new ServerAsyncTask();
		
		details.setUrl(url);
		details.putParameter("rideid", rideId);
		
		String resultLines[] = null;
		String result = null;
		try {
			result = getDetails.execute(details).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(result != null){
			resultLines = result.split(",");
		}
		else{
			Toast.makeText(this, "Unavailable Network", Toast.LENGTH_LONG).show();
		}
		
		return resultLines;
		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start_ride, menu);
		return true;
	}

	public void onClickStartRideButton(View view){
		asyncStartRide();
		Intent intent = new Intent(this, StopRideActivity.class);
		intent.putExtra("RIDE_ID", rideId);
		intent.putExtra("FROM", fromLocation);
		intent.putExtra("TO", toLocation);
		startActivity(intent);
		finish();
	}
	 private void asyncStartRide()
	    {
	    
	    	ServerRequestObject startRide = new ServerRequestObject();
	    	startRide.setUrl("http://172.9.69.232/wheelshare/startstopride.php");

	    	startRide.putParameter("uid", uid);
	    	startRide.putParameter("rideid", rideId);
	    	startRide.putParameter("action", "start");
	    	
	    	ServerAsyncTask requestRide = new ServerAsyncTask();
	    	try{
	    		String str = requestRide.execute(startRide).get();
		    	Toast.makeText(this,  "Result Code: " + str, Toast.LENGTH_LONG).show();
	    	
	    	}catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	
	protected void showCurrentLocation() {
		List<Address> addresses;
		Address address;
		String locality = "Currently in: ";
    	Location location = mgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
    	Geocoder translator = new Geocoder(this);
    	
    	try {
			addresses = translator.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
			address = addresses.get(0);
	    	locality = locality + address.getLocality();
	    	TextView currentLocation = (TextView) this.findViewById(R.id.locationText);
		    currentLocation.setText(locality);
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
   } 
	
	
	LocationListener onLocationChange=new LocationListener() {
	    public void onLocationChanged(Location location) {
	     
			@SuppressWarnings("unused")
	    	Location targetLocation = mgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);//null;
	    			
	    	showCurrentLocation();
	    	//Toast.makeText(MainGpsActivity.this, message, Toast.LENGTH_LONG).show();	    
	    }
	    
	    public void onProviderDisabled(String provider) {
	      // required for interface, not used
	    
	    }
	    
	    public void onProviderEnabled(String provider) {
	      // required for interface, not used
	    	
	    }
	    
	    public void onStatusChanged(String provider, int status,
	                                  Bundle extras) {
	      // required for interface, not used
	    	
	    }
	 };
	
}
