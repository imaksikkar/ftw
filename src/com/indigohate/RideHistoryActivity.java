package com.indigohate;

import com.indigohate.SearchResultsListAdapter;


import java.util.ArrayList;
import com.indigohate.dummy.DummyContent;
import android.os.Bundle;
import android.app.ListActivity;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import java.util.concurrent.ExecutionException;


public class RideHistoryActivity extends ListActivity {

	//private ListView rideHistoryListView;
	public String uid;
	public String ridedataResult;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    //private Callbacks mCallbacks = sDummyCallbacks;
    private Callbacks mCallbacks = sRideCallbacks;
    private int mActivatedPosition = ListView.INVALID_POSITION;
    private ListView searchResultsList;
    
    public interface Callbacks {

        public void onItemSelected(String id);
    }
    
    private static Callbacks sRideCallbacks = new Callbacks(){
    	@Override
    	public void onItemSelected(String id){}
    };
    
    public RideHistoryActivity(){
    }
    

    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ride_history);
		asyncGetRideHistory();
		
	}

	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
    }
    

	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ride_history, menu);
		return true;
	}
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        mCallbacks.onItemSelected(DummyContent.ITEMS.get(position).id);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    public void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }
    
    private void asyncGetRideHistory()
    {
    	ArrayList<String> dataLinesList = new ArrayList<String>();
    	ServerRequestObject rideHistoryData = new ServerRequestObject();
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    	uid = settings.getString("UID", "Invalid");
    	rideHistoryData.setUrl("http://172.9.69.232/wheelshare/ridehistory.php?id=" + uid);
    	ServerAsyncTask getRideHistory = new ServerAsyncTask();
    	try{
    		String str = getRideHistory.execute(rideHistoryData).get();
    		String[] ridedataLines = str.split(";");
    		try{
    			 for(int i=0; i < ridedataLines.length; i++){
    				dataLinesList.add(ridedataLines[i]);
    			 }
    			
    			}catch(NullPointerException e){
    				e.printStackTrace();
    			}
    		
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	searchResultsList = getListView();
		//searchResultsList = (ListView)findViewById(R.id.rideHistoryListView);
		
		SearchResultsListAdapter adapter = new SearchResultsListAdapter(this,
				//R.layout.listview_item_row, 
				android.R.layout.simple_list_item_1,
				dataLinesList);
		
		searchResultsList.setAdapter(adapter);
    }
}
