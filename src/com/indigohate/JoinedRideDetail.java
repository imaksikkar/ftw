package com.indigohate;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.indigohate.classes.InputCheck;

import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class JoinedRideDetail extends Activity {
	public String rideDataItems[];
	public String uid;
	public String rideId;
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_joined_ride_detail);		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);		
		uid = settings.getString("UID", "Invalid");
		
		
		String detailItems[] = null;
		@SuppressWarnings("unused")
		String rideDetails = null;
		
		rideId = getIntent().getStringExtra("rideId");
		String owner = getIntent().getStringExtra("owner");
		String date = getIntent().getStringExtra("date");
		String time = getIntent().getStringExtra("time");
		String startLocation =getIntent().getStringExtra("startLocation");
		String destination = getIntent().getStringExtra("destination");
		String price = getIntent().getStringExtra("price");
		String seatsLeft = getIntent().getStringExtra("seatsLeft");
		//detailItems = getRideDetails(rideId);
		TextView txtOwner = (TextView) findViewById(R.id.joined_details_owner);
		TextView txtDate = (TextView) findViewById(R.id.joined_details_date);
		TextView txtFrom = (TextView) findViewById(R.id.joined_details_from);
		TextView txtTo = (TextView) findViewById(R.id.joined_details_to);
		TextView txtCost = (TextView) findViewById(R.id.joined_details_cost);
		TextView txtSeats = (TextView) findViewById(R.id.joined_details_seats_left);
		Toast.makeText(getBaseContext(), rideId, Toast.LENGTH_LONG).show();
		txtOwner.setText(owner);
		txtDate.setText(date);
		txtFrom.setText(startLocation);
		txtTo.setText(destination);
		txtCost.setText(price);
		txtSeats.setText(seatsLeft);
		
		/*
		String rideData = getIntent().getStringExtra("ride");
		rideDataItems = rideData.split(" ");

		rideId = rideDataItems[0];
		detailItems = getRideDetails(rideId);
		Log.d("JoinedRideDetails", "detailItems length: "+String.valueOf(detailItems.length));

		TextView owner = (TextView) findViewById(R.id.joined_details_owner);
		TextView date = (TextView) findViewById(R.id.joined_details_date);
		TextView from = (TextView) findViewById(R.id.joined_details_from);
		TextView to = (TextView) findViewById(R.id.joined_details_to);
		TextView cost = (TextView) findViewById(R.id.joined_details_cost);
		TextView seats = (TextView) findViewById(R.id.joined_details_seats_left);
		
		Toast.makeText(getBaseContext(), detailItems[0], Toast.LENGTH_LONG).show();
		
		if(detailItems.length > 11){
		owner.setText(detailItems[1]);
		date.setText(detailItems[6]);
		from.setText(detailItems[4]);
		to.setText(detailItems[5]);
		cost.setText(detailItems[11]);
		seats.setText(detailItems[2]);
		}*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.joined_ride_detail, menu);
		return true;
	}

	private String[] getRideDetails(String rideId){
		String url = "http://172.9.69.232/wheelshare/getridebyidpost.php";
		
		ServerRequestObject details = new ServerRequestObject();
		ServerAsyncTask getDetails = new ServerAsyncTask();
		
		details.setUrl(url);
		details.putParameter("rideid", rideId);
		
		String resultLines[] = null;
		String result = null;
		try {
			result = getDetails.execute(details).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(result != null){
			resultLines = result.split(",");
		}
		
		return resultLines;	
	}
	
	public void onStartRideButtonClick(View view) {
		Intent intent = new Intent(this, StartRideActivity.class);
		intent.putExtra("RIDE_ID", rideId);
		startActivity(intent);
	}
	
	public void onChatButtonClick(View view){
		Intent intent = new Intent(this, ChatActivity.class);
		intent.putExtra("rideid", rideId);
		startActivity(intent);
		
		;
	}
	
}
