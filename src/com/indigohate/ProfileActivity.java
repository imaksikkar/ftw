package com.indigohate;



import java.util.concurrent.ExecutionException;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ProfileActivity extends Activity {

	public String userdataResult;
	public String profileIdentifier = "profile";
	public String uid;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.PROFILE";
	
	private void asyncGetProfileData()
	{
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		uid = settings.getString("UID", "Invalid");
		ServerRequestObject getProfileData = new ServerRequestObject();
		getProfileData.setUrl("http://172.9.69.232/wheelshare/profile.php?id=" + uid);
		ServerAsyncTask getProfile = new ServerAsyncTask();
		try{
			String str = getProfile.execute(getProfileData).get();
			String[] userdataLines = str.split(",");
			
			setContentView(R.layout.activity_profile);
			
			EditText inputFirstname = (EditText) findViewById(R.id.textview_first_name); 
			EditText inputLastname = (EditText) findViewById(R.id.textview_last_name);
			EditText inputStreet = (EditText) findViewById(R.id.textview_address);
			EditText inputCity = (EditText) findViewById(R.id.textview_city);
			EditText inputState = (EditText) findViewById(R.id.textview_state);
			EditText inputZip = (EditText) findViewById(R.id.textview_zip_code);
			
			inputFirstname.setText(userdataLines[2]);
			inputLastname.setText(userdataLines[3]);
			inputStreet.setText(userdataLines[4]);
			inputCity.setText(userdataLines[5]);
			inputState.setText(userdataLines[6]);
			inputZip.setText(userdataLines[7]);
		
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		asyncGetProfileData();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}
	
	public void paymentInfoClick(View view)
	{
		Intent intent = new Intent(this, PaymentInformationActivity.class);
		intent.putExtra("LAST_ACTIVITY", profileIdentifier);
		startActivity(intent);
	}
	
	public void rideHistoryClick(View view)
	{
		Intent intent = new Intent(this, RideHistoryActivity.class);
		startActivity(intent);
		
	}
	
	public void saveChangesClick(View view)
	{
		//updateContact();
		asyncUpdateContact();
	}
	
	public void onClickMakeCommentButton(View view){
		Intent intent = new Intent(this, MakeCommentActivity.class);
		startActivity(intent);
	}
	
	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	private void asyncUpdateContact()
	{
		ServerRequestObject updateProfileData = new ServerRequestObject();
		updateProfileData.setUrl("http://172.9.69.232/wheelshare/updateprofile.php");
		EditText inputFirstname = (EditText) findViewById(R.id.textview_first_name); 
		EditText inputLastname = (EditText) findViewById(R.id.textview_last_name);
		EditText inputStreet = (EditText) findViewById(R.id.textview_address);
		EditText inputCity = (EditText) findViewById(R.id.textview_city);
		EditText inputState = (EditText) findViewById(R.id.textview_state);
		EditText inputZip = (EditText) findViewById(R.id.textview_zip_code);
		
		String firstName = inputFirstname.getText().toString();
		String lastName = inputLastname.getText().toString();
		String street = inputStreet.getText().toString();
		String city = inputCity.getText().toString();
		String state = inputState.getText().toString();
		String zip = inputZip.getText().toString();
		
		updateProfileData.putParameter("firstname", firstName);
		updateProfileData.putParameter("lastname", lastName);
		updateProfileData.putParameter("uid", uid);
		updateProfileData.putParameter("street", street);
		updateProfileData.putParameter("city", city);
		updateProfileData.putParameter("state", state);
		updateProfileData.putParameter("zip", zip);
		
		ServerAsyncTask updateProfile = new ServerAsyncTask();
		try{
			String str = updateProfile.execute(updateProfileData).get();
			Toast.makeText(this,  "Result: " + str, Toast.LENGTH_LONG).show();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	
}
