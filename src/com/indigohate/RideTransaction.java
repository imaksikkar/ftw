package com.indigohate;

import java.util.ArrayList;
import java.util.Date;

public class RideTransaction {
	public String firstName;
	public String lastName;
	public ArrayList<String> passengers;
	//some kind promptness rating..?
	public String startLocation;
	public String endLocation;
	public Date dateTime;
	

	public RideTransaction()
	{
		super();
	}
	
	public RideTransaction(String firstName, String lastName, ArrayList<String> passengers, String startLocation, String endLocation, Date dateTime)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.passengers = passengers;
		this.startLocation = startLocation;
		this.endLocation = endLocation;
		this.dateTime = dateTime;
	}
}
