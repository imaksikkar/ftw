package com.indigohate;

import android.os.Bundle;
import android.app.ListActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.content.Intent;
import android.content.SharedPreferences;



public class AllRidesListActivity extends ListActivity {
	public String uid;
	public String ridedataResult;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
	@SuppressWarnings("unused")
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    //private Callbacks mCallbacks = sDummyCallbacks;
	
    @SuppressWarnings("unused")
	private Callbacks mCallbacks = sRideCallbacks;
	
    @SuppressWarnings("unused")
	private int mActivatedPosition = ListView.INVALID_POSITION;
    private ListView searchResultsList;
    
    public interface Callbacks {

        public void onItemSelected(String id);
    }
    
    private static Callbacks sRideCallbacks = new Callbacks(){
    	@Override
    	public void onItemSelected(String id){}
    };
    
    public AllRidesListActivity(){
    	
    }
    

    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_rides_list);
		asyncGetMyRides();
	
		
	}

	public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.all_rides_list, menu);
		return true;
	}

	
    private void asyncGetMyRides()
    {
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    	uid = settings.getString("UID", "Invalid");
		
		String url = "http://172.9.69.232/wheelshare/allridespost.php";;
    	ServerRequestObject rideData = new ServerRequestObject();
    	rideData.setUrl(url);
    	rideData.putParameter("uid", uid);
    	ArrayList<String> dataLinesList = new ArrayList<String>();
    	ServerAsyncTask getRides = new ServerAsyncTask();
    	try{
    		String str = getRides.execute(rideData).get();

			Log.d("ALLRIDES","str is not eof. its |" + str + "|");
			String[] ridedataLines = str.split(";");
		
    		try{
    			 for(int i=0; i < ridedataLines.length; i++){
    				 if(ridedataLines[i].startsWith("EOF"))
    					 break;
    				 else
    					 dataLinesList.add(ridedataLines[i]);
    			 }
    			
    			}catch(NullPointerException e){
    				e.printStackTrace();
    				//maybe change this to a conditional?
    			}
    		
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		searchResultsList = getListView();
		//searchResultsList = (ListView)findViewById(R.id.rideHistoryListView);
		
		SearchResultsListAdapter adapter = new SearchResultsListAdapter(this,
				//R.layout.listview_item_row, 
				android.R.layout.simple_list_item_1,
				dataLinesList);
		
		searchResultsList.setAdapter(adapter);
    }
    
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        Intent intent;
        String item = ((TextView)view).getText().toString();
        
        //Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
        String lines[] = item.split(" ");
        if(lines[1].equals("Joined")){
        	intent = new Intent(this, JoinedRideDetail.class);
        }else{
        
        	intent = new Intent(this, ExistingOfferOrRequestActivity.class);
        }
        
        Bundle extras = new Bundle();
        extras.putString("ride", item);
        intent.putExtras(extras);
        
        startActivity(intent);
        
    }
    
}
