package com.indigohate;


import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


import com.indigohate.SearchResultsListAdapter;


import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class SearchResultsListActivity extends ListActivity {
	
	public String uid;
	public String ridedataResult;
	public static final String EXTRA_UID = "com.indigohate.extra.UID";
	public static final String PREFS_NAME = "WheelsharePrefsFile";
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
	@SuppressWarnings("unused")
    private Callbacks mCallbacks = sRideCallbacks;
    private int mActivatedPosition = ListView.INVALID_POSITION;
    private ListView searchResultsList;
    boolean checked = true;
	boolean unchecked = false;
    
    public interface Callbacks {

        public void onItemSelected(String id);
    }

    private static Callbacks sRideCallbacks = new Callbacks(){
    	@Override
    	public void onItemSelected(String id){}
    };
    
    public SearchResultsListActivity() {
    }

    
    @Override
    public void onCreate(Bundle extras) {
        super.onCreate(extras);
        setContentView(R.layout.activity_search_results);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);		
		uid = settings.getString("UID", "Invalid");
		
		String doRequest = getIntent().getStringExtra("request");
		if(doRequest != null){
			//requestAndSearch();
			asyncRequestAndSearch();
		}else{
			asyncDoSearch();
			//doSearch();
		}
       
    }
    
    public void onClickSortByPrice(View view)
    {
    	CheckBox ratingCheck = (CheckBox)findViewById(R.id.sortByRatingCheck);
    	if(ratingCheck.isChecked()){
    		ratingCheck.setChecked(unchecked);
    		//Reset server request for no filter here
    	}
    	
    }
    
    public void onClickSortByRating(View view)
    {
    	
    	CheckBox priceCheck = (CheckBox)findViewById(R.id.sortByPriceCheck);
    	if(priceCheck.isChecked()){
    		priceCheck.setChecked(unchecked);
    		//Reset server request for no filter here
    	}

    }
    
    private void asyncRequestRide()
    {
    	ServerRequestObject rideData = new ServerRequestObject();
    	rideData.setUrl("http://172.9.69.232/wheelshare/requestride.php");
    	String fromLocation = getIntent().getStringExtra("startLocation");
    	String toDestination = getIntent().getStringExtra("endLocation");
    	String rideDate = getIntent().getStringExtra("rideDate");
    	String rideTime = getIntent().getStringExtra("rideTime");
    	rideData.putParameter("fromlocation", fromLocation);
    	rideData.putParameter("todestination", toDestination);
    	rideData.putParameter("fromdate", rideDate);
    	rideData.putParameter("requesterid", uid);
    	rideData.putParameter("starttime", rideTime);
    	ServerAsyncTask requestRide = new ServerAsyncTask();
    	try{
    		String str = requestRide.execute(rideData).get();
    		Toast.makeText(this,  "Result Code: " + str, Toast.LENGTH_LONG).show();
    		
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void onRequestButtonClick(View view){
    	asyncRequestRide();
    	asyncRequestAndSearch();
    	
    }
    
    private void asyncDoSearch()
    {
    	ServerRequestObject rideData = new ServerRequestObject();
    	rideData.setUrl("http://172.9.69.232/wheelshare/dosearch.php");
    	
    	String fromLocation = getIntent().getStringExtra("startLocation");
    	String toDestination = getIntent().getStringExtra("endLocation");
    	String rideDate = getIntent().getStringExtra("rideDate");
    	String rideTime = getIntent().getStringExtra("rideTime");
    	
    	rideData.putParameter("fromlocation", fromLocation);
    	rideData.putParameter("todestination", toDestination);
    	rideData.putParameter("fromdate", rideDate);
    	rideData.putParameter("starttime", rideTime);
    	ServerAsyncTask searchRide = new ServerAsyncTask();
    	ArrayList<String> dataLinesList = new ArrayList<String>();
    	try{
    		String str = searchRide.execute(rideData).get();
    		String[] ridedataLines = str.split(";");
    		try{
    			
   			 for(int i=0; i < ridedataLines.length; i++){
   				dataLinesList.add(ridedataLines[i]);
   			 }
   			
   			}catch(NullPointerException e){
   				e.printStackTrace();
   			}
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	searchResultsList = getListView();
		
		SearchResultsListAdapter adapter = new SearchResultsListAdapter(this,
				android.R.layout.simple_list_item_1,
				dataLinesList);
		searchResultsList.setAdapter(adapter);
    }

    
    
    private void asyncRequestAndSearch()
    {
    	ArrayList<String> dataLinesList = new ArrayList<String>();
    	ServerRequestObject rideData = new ServerRequestObject();
    	rideData.setUrl("http://172.9.69.232/wheelshare/searchride.php?id=" + uid);
    	ServerAsyncTask requestAndSearchRide = new ServerAsyncTask();
    	try{
    		String str = requestAndSearchRide.execute(rideData).get();
    		String[] ridedataLines = str.split(";");
    		try{
    			
   			 for(int i=0; i < ridedataLines.length; i++){
   				dataLinesList.add(ridedataLines[i]);
   			 }
   			
   			}catch(NullPointerException e){
   				e.printStackTrace();
   			}
    	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	searchResultsList = getListView();
		
		SearchResultsListAdapter adapter = new SearchResultsListAdapter(this,
				android.R.layout.simple_list_item_1,
				dataLinesList);
		searchResultsList.setAdapter(adapter);
    }
    

    
    
    public static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
    }
    

   

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        String item = ((TextView)view).getText().toString();
        
        //Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
        
        Intent intent = new Intent(this, RideDetailActivity.class);
        Bundle extras = new Bundle();
        extras.putString("ride", item);
        intent.putExtras(extras);
        
        startActivity(intent);
        
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    public void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }
    
    
/*
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null && savedInstanceState
                .containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    public void onDetach() {
        super.onDetach();
        mCallbacks = sRideCallbacks;
    }
*/
    
    /*
    private static String[] parseRideData(String rideDataStrings[]){
    	//Returns an array of type Ride from the lines of data returned by the server
    	
    	String rideData[] = null;
    	
    	String dataLines[] = null;
    	for(int j=0; j < 13; j++){
    		dataLines[j] = " ";
    	}
    	
    	for(int i=0; i < rideDataStrings.length; i++){
    		dataLines = rideDataStrings[i].split(",");
    		
    		rideData[i] = "Departs at: " + dataLines[12] + " from: " + dataLines[4] +
    				" to: " + dataLines[5] + " on: " + dataLines[6];
    		
    	}
    	
    	
    	return rideData;
    }   */
    
}